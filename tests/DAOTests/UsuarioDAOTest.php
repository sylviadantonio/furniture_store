<?php
namespace Tests\DAOTests;

use sampleStructure\DAO;
use sampleStructure\Model;
use sampleStructure\Utils;
use PHPUnit\Framework\TestCase;


class UsuarioDAOTest extends TestCase {

  private $perfilDAO;
  private $dao;
  private $mock;

  public function __construct() {
    $this->mock = $this->createMock(\Monolog\Logger::class);
    $this->dao = new DAO\UsuarioDAO($this->mock);
    $this->perfilDAO = new DAO\PerfilDAO($this->mock);
    $this->assertNotNull($this->dao->getLogger());
    $this->assertTrue($this->dao->isTest());
  }

  public function testeAchouDAO (){
    $dao = new DAO\UsuarioDAO(null);
    $perfilDAO = new DAO\PerfilDAO(null);

    print("\nTestando Usuario DAO");
    $this->assertTrue($dao->isTest(), 'Não é de teste');

    print("\nTestando Usuario Log");
    $mock = $this->createMock(\Monolog\Logger::class);
    $dao = new DAO\UsuarioDAO($mock);
    $this->assertNotNull($dao->getLogger(), 'Nao achou logger');
  }

  public function testaUsuario() {
    print("\n==============================");
    print("\nTestes Usuario");
    print("\n==============================");

    $idPerfil = $this->insereNovoPerfil();
    $usuario = $this->criaUsuario();
    $usuario->idperfil = $idPerfil;
    $id = $this->gravaUsuario($usuario);
    $this->assertTrue($id != "", "Erro ao gravar usuario");

    $total = $this->dao->recuperaQuantidadeUsuariosAtivos();
    $this->assertNotNull($total, 'Nao achou logger');
    $this->assertTrue($total[0]->total == 2);

    $executed = $this->apagaUsuario($id);
    $this->assertTrue($executed != 0, "Erro ao apagar usuario");

    $this->apagaPerfil($idPerfil);

    print("\n==============================");
    print("\nFim de Testes de Usuario");
    print("\n==============================");
  }

  public function testaLogin() {
    print("\n==============================");
    print("\nTestes Login");
    print("\n==============================");

    $idPerfil = $this->insereNovoPerfil();
    $usuario = $this->criaUsuario();
    $usuario->idperfil = $idPerfil;
    $id = $this->gravaUsuario($usuario);
    $this->assertTrue($id != "", "Erro ao gravar usuario");

    print("\nTestando funcao de login -- senha errada\n");
    $usuario->senha = "654321";
    $result = $this->dao->recuperaLogin($usuario);
    $this->assertTrue(!$result, "Login com senha errada retornou algo");

    print("\nTestando funcao de login -- correto");
    $usuario->senha = "123456";
    $result = $this->dao->recuperaLogin($usuario);
    $this->assertTrue($result != false, "Login com dados corretos retornaram errado");

    $executed = $this->apagaUsuario($id);
    $this->assertTrue($executed != 0, "Erro ao apagar usuario");

    $this->apagaPerfil($idPerfil);
    print("\n==============================");
    print("\nFim de Testes de Login");
    print("\n==============================");
  }


  private function insereNovoPerfil() {
      print("\nCriando novo perfil");
      $id = $this->perfilDAO->gravaPerfil("Teste");
      $this->assertTrue(!empty($id), "erro ao inserir novo perfil dentro de testes de usuario.");

      return $id;
  }

  private function apagaPerfil($idPerfil) {
    print("\nApagando perfil");
    $executed = $this->perfilDAO->apagaPerfil($idPerfil);

    $this->assertTrue($executed != 0, "erro ao apagar perfil dentro de testes de usuario.");
  }

  private function criaUsuario () {
    print("\nCriando novo Usuario");
    $usuario = new \stdClass();
    $usuario->nome = "Teste";
    $usuario->email = "teste@teste123.com";
    $usuario->senha = Utils\Utils::encryptPassword("123456");
    $usuario->datagravacao = date('Y-m-d H:i:s',time());

    return $usuario;
  }

  private function gravaUsuario($usuario) {
    print("\nTestando Gravar Usuario");
    return $this->dao->gravaUsuario($usuario);
  }

  private function apagaUsuario($id) {
    print("\nTestando Apagar Usuario");
    return $this->dao->apagaUsuario($id);
  }
}

<?php
namespace Tests\Functional;

use sampleStructure\DAO;
use PHPUnit\Framework\TestCase;

class FuncionalidadeDAOTest extends TestCase {
  private $dao;
  private $mock;

  public function __construct() {
    $this->mock = $this->createMock(\Monolog\Logger::class);
    $this->dao = new DAO\FuncionalidadeDAO($this->mock);
    $this->assertNotNull($this->dao->getLogger());
    $this->assertTrue($this->dao->isTest());
  }

  public function testGravaFuncionalidade(){
      $this->assertNotNull($this->dao->getLogger());

      $id = $this->dao->gravaFuncionalidade("teste", "nome", 1, 1);
      $this->assertTrue($id != "");
      $func = $this->dao->recuperaFuncionalidadePorId($id);
      $this->assertNotNull($func);
      $func = $func[0];

      $this->assertTrue($func->nome == "nome");
      $this->assertTrue($func->url == "teste");

      $this->dao->apagaFuncionalidade($id);
  }

  public function testApagaFuncionalidade(){
      $this->assertNotNull($this->dao->getLogger());

      $id = $this->dao->gravaFuncionalidade("teste", "nome", 1, 1);
      $this->assertTrue($id != "");
      $this->dao->apagaFuncionalidade($id);
  }

  public function testRecuperaTodasFuncionalidades(){
      $this->assertNotNull($this->dao->getLogger());

      $lista = $this->dao->recuperaTodasFuncionalidades();
      $this->assertNotEmpty($lista);

      $id = $this->dao->gravaFuncionalidade("teste", "nome", 1, 1);
      $this->assertTrue($id != "");

      $lista = $this->dao->recuperaTodasFuncionalidades();
      $this->assertNotEmpty($lista);

      $this->dao->apagaFuncionalidade($id);
  }

  public function testRecuperaTodasCategoriasMenu(){
    $lista = $this->dao->recuperaTodasCategoriasMenu();
    $this->assertNotEmpty($lista);
  }

  public function testRecuperaPermissaoPorIdPerfil(){
      $this->assertNotNull($this->dao->getLogger());

      $lista = $this->dao->recuperaPermissaoAcessoPorIdPerfil(null);
      $this->assertEmpty($lista);

      $this->daoP = new DAO\PerfilDAO($this->mock);
      $this->assertNotNull($this->daoP->getLogger());
      $this->assertTrue($this->daoP->isTest());

      $idPerfil = $this->daoP->gravaPerfil("testep");
      $this->assertNotNull($idPerfil);

      $id = $this->dao->gravaFuncionalidade("teste", "nome", 1, 1);
      $this->assertTrue($id != "");

      $idPermissao = $this->dao->gravaPermissaoAcesso($id, $idPerfil);
      $this->assertNotNull($idPermissao);

      $lista = $this->dao->recuperaPermissaoAcessoPorIdPerfil($idPerfil);
      $this->assertNotEmpty($lista);

      $this->dao->apagaPermissaoAcessoPorIdFuncionalidade($id);
      $this->dao->apagaFuncionalidade($id);
      $this->daoP->apagaPerfil($idPerfil);
  }

}
?>

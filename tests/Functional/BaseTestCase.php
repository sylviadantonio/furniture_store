<?php

namespace Tests\Functional;

use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\Environment;
use PHPUnit\Framework\TestCase;

class BaseTestCase extends TestCase
{
    /**
     * Use middleware when running application?
     *
     * @var bool
     */
    protected $withMiddleware = true;
    public $validateRoutes = true;

    /**
     * Process the application given a request method and URI
     *
     * @param string $requestMethod the request method (e.g. GET, POST, etc.)
     * @param string $requestUri the request URI
     * @param array|object|null $requestData the request data
     * @return \Slim\Http\Response
     */
    public function runApp($requestMethod, $requestUri, $requestData = null)
    {
      putenv("TEST=true");
      $_ENV["TEST"] = true;
      putenv("SKIP=$this->validateRoutes");
      $_ENV["SKIP"] = $this->validateRoutes;
        // Create a mock environment for testing with
        $environment = Environment::mock(
            [
                'REQUEST_METHOD' => $requestMethod,
                'REQUEST_URI' => $requestUri
            ]
        );
                @session_start();
        // Set up a request object based on the environment
        $request = Request::createFromEnvironment($environment);

        // Add request data, if it exists
        if (isset($requestData)) {
            $request = $request->withParsedBody($requestData);
        }

        // Set up a response object
        $response = new Response();

        // Use the application settings
        $settings = require __DIR__ . '/../../src/settings.php';

        // Instantiate the application
        $app = new App($settings);

        // Set up dependencies
        $dependencies = require __DIR__ . '/../../src/dependencies.php';
        $dependencies($app);

        // Register middleware
        if ($this->withMiddleware) {
            $middleware = require __DIR__ . '/../../src/middleware.php';
            $middleware($app);
        }

        // Register routes
        $routes = require __DIR__ . '/../../src/routes.php';
        $routes($app);
        //print(var_dump($request));die;

        // Process the application
        $response = $app->process($request, $response);

        // Return the response
        return $response;
    }

    protected function assertFlash($level, $message)
    {

        $expectedNotification = [
            $level => array()
        ];
        $expectedNotification[$level][0] = $message;

        //$expectedNotification = json_encode($expectedNotification);
        $flashNotifications = $_SESSION['slimFlash'][$level];
        //print(var_dump($expectedNotification));
        //print(var_dump($_SESSION['slimFlash']));
        //print($flashNotifications[0]);
        //print($message);
        if (! $flashNotifications) {
            $this->fail('Failed asserting that a flash message was sent.');
        }
        $this->assertContains(
            $flashNotifications[0],
            $message,
            "Failed asserting that the flash message '$message' is present."
        );
    }

}

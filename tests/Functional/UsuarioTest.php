<?php
namespace Tests\Functional;

use sampleStructure\DAO;
use sampleStructure\Utils\Utils;

class UsuarioTest extends BaseTestCase
{
  private $dao;
  private $daoPerfil;
  private $helperDao;
  private $logger;
  private $emailTeste = "teste@teste.com";

  public function __construct() {
    $this->logger = new \Monolog\Logger('testes funcionais de usuários');
    $mock = $this->createMock(\Monolog\Logger::class);
    $this->dao = new DAO\UsuarioDAO($this->logger);
    @session_start();
  }

    public function testPaginaInicial()
    {
        $response = $this->runApp('GET', '/usuario');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testGravar() {
      print("\n========================================");
      print("\nTeste Usuario Controller gravação");
      print("\n========================================");
      $usuario = new \stdClass();
      $usuario->idusuario = "";
      $_SESSION['usuario'] = $usuario;
      $response = $this->runApp('POST', '/usuario', $usuario);
      if($response->getStatusCode() == 500){
        print($response->getBody());
      }
      $this->assertEquals(302, $response->getStatusCode());
      $msg = "É necessário preencher o nome do usuário.";
      $msg .= "É necessário preencher o email do usuário.";
      $this->assertFlash("error", $msg);

      $usuario->nome = "teste";
      $response = $this->runApp('POST', '/usuario', $usuario);
      if($response->getStatusCode() == 500){
        print($response->getBody());
      }

      $msg .= "É necessário preencher o email do usuário.";
      $this->assertEquals(302, $response->getStatusCode());
      $this->assertFlash("error", $msg);

      $usuario->email = "123";
      $response = $this->runApp('POST', '/usuario', $usuario);
      if($response->getStatusCode() == 500){
        print($response->getBody());die;
      }

      $msg = "O email informado não tem um formato válido.";
      $this->assertEquals(302, $response->getStatusCode());
      $this->assertFlash("error", $msg);

      $usuario->email = "teste@teste.com";
      $response = $this->runApp('POST', '/usuario', $usuario);
      if($response->getStatusCode() == 500){
        print($response->getBody());die;
      }

      $msg = "Senha é obrigatória.";
      $this->assertEquals(302, $response->getStatusCode());
      $this->assertFlash("error", $msg);

      $usuario->senha = "123";
      $response = $this->runApp('POST', '/usuario', $usuario);
      if($response->getStatusCode() == 500){
        print($response->getBody());die;
      }
      $msg = "Senha não pode ter menos de 6 caracteres.";
      $this->assertEquals(302, $response->getStatusCode());
      $this->assertFlash("error", $msg);

      $usuario->senha = "1234567";
      $response = $this->runApp('POST', '/usuario', $usuario);
      if($response->getStatusCode() == 500){
        print($response->getBody());die;
      }
      $msg = "Confirme sua senha.";
      $this->assertEquals(302, $response->getStatusCode());
      $this->assertFlash("error", $msg);

      $usuario->confirmaSenha = "12347";
      $response = $this->runApp('POST', '/usuario', $usuario);
      if($response->getStatusCode() == 500){
        print($response->getBody());die;
      }
      $msg = "Senhas estão diferentes.";
      $this->assertEquals(302, $response->getStatusCode());
      $this->assertFlash("error", $msg);

      $usuario->confirmaSenha = "1234567";
      $response = $this->runApp('POST', '/usuario', $usuario);
      if($response->getStatusCode() == 500){
        print($response->getBody());die;
      }
      $msg = "É obrigatório escolher um perfil.";
      $this->assertEquals(302, $response->getStatusCode());
      $this->assertFlash("error", $msg);

      $usuario->idperfil = "1";
      $response = $this->runApp('POST', '/usuario', $usuario);
      if($response->getStatusCode() == 500){
        print($response->getBody());die;
      }
      $this->assertEquals(302, $response->getStatusCode());
      $this->assertFlash("sucess", "Usuário cadastrado com sucesso.");

      $response = $this->runApp('POST', '/usuario', $usuario);
      if($response->getStatusCode() == 500){
        print($response->getBody());die;
      }
      $this->assertEquals(302, $response->getStatusCode());
      $this->assertFlash("error", "Já existe um usuário com este e-mail.");

      $func = $this->dao->recuperaUsuarioPorEmail($usuario->email);
      $this->assertNotNull($func);
      $this->assertTrue(count($func) == 1);
      $func = $func[0];

      $this->apagaUsuario($func->idusuario);
      $_SESSION['usuario'] = NULL;
    }

    public function testApagar() {
      print("\n========================================");
      print("\nTeste Usuario Controller apagar");
      print("\n========================================");
      $this->gravaUsuario();

      $func = $this->dao->recuperaUsuarioPorEmail($this->emailTeste);
      $this->assertNotNull($func);
      $this->assertTrue(count($func) == 1);
      $func = $func[0];

      $url = '/usuario/' . $func->idusuario;
      $response = $this->runApp('POST', $url, $func);
      if($response->getStatusCode() == 500){
        print($response->getBody());die;
        $this->apagaUsuario($func->idusuario);
        $_SESSION['usuario'] = NULL;
      }

      $this->assertEquals(302, $response->getStatusCode());
      $this->assertFlash("sucess", "Usuário apagado com sucesso.");
    }

    public function testEditar() {
      $this->logger->info("integração edição de usuario");
      print("\n========================================");
      print("\nTeste Usuario Controller editar");
      print("\n========================================");
      $this->gravaUsuario();
      $usuario = $this->dao->recuperaUsuarioPorEmail($this->emailTeste);
      $this->assertNotNull($usuario);
      $this->assertTrue(count($usuario) == 1);
      $usuario = $usuario[0];
      $response = $this->runApp('GET', '/usuario/'.$usuario->idusuario);
      if($response->getStatusCode() == 500){
        print($response->getBody());die;
        $this->apagaUsuario($usuario->idusuario);
        $_SESSION['usuario'] = NULL;
      }
      $this->assertEquals(200, $response->getStatusCode());

      $usuario->nome = "teste 2";
      $usuario->confirmaSenha = "123456";
      $usuario->senha = "123456";
      $response = $this->runApp('POST', '/usuario', $usuario);
      if($response->getStatusCode() == 500){
        print($response->getBody());die;
        $this->apagaUsuario($usuario->idusuario);
        $_SESSION['usuario'] = NULL;
      }

      $this->assertEquals(302, $response->getStatusCode());
      $this->assertFlash("sucess", "Usuário atualizado com sucesso.");

      $this->gravaUsuario("teste2@teste.com");
      $usuarioNovo = $this->dao->recuperaUsuarioPorEmail("teste2@teste.com");
      $this->assertNotNull($usuarioNovo);
      $this->assertTrue(count($usuarioNovo) == 1);
      $usuarioNovo = $usuarioNovo[0];

      $usuarioNovo->email = "teste@teste.com";
      $usuarioNovo->confirmaSenha = "123456";
      $usuarioNovo->senha = "123456";
      $response = $this->runApp('POST', '/usuario', $usuarioNovo);
      if($response->getStatusCode() == 500){
        print($response->getBody());die;
        $this->apagaUsuario($usuarioNovo->idusuario);
        $_SESSION['usuario'] = NULL;
      }
      $this->assertEquals(302, $response->getStatusCode());
      $this->assertFlash("error", "Já existe um usuário com este e-mail.");

      $usuarioNovo->email = "teste@teste.com";
      $response = $this->runApp('POST', '/usuario', $usuarioNovo);
      if($response->getStatusCode() == 500){
        print($response->getBody());die;
        $this->apagaUsuario($usuarioNovo->idusuario);
        $_SESSION['usuario'] = NULL;
      }
      $this->assertEquals(302, $response->getStatusCode());
      $this->assertFlash("error", "Já existe um usuário com este e-mail.");

      $this->logger->info("teste de edição de usuário sem atualizar a senha");
      $usuario = $this->dao->recuperaUsuarioPorEmail($this->emailTeste);
      $this->assertNotNull($usuario);
      $this->assertTrue(count($usuario) == 1);
      $usuario = $usuario[0];
      $usuario->nome = "teste 123";
      $senhaAntiga = $usuario->senha;
      $usuario->confirmaSenha = "";
      $usuario->senha = "";
      $response = $this->runApp('POST', '/usuario', $usuario);
      if($response->getStatusCode() == 500){
        print($response->getBody());die;
        $this->apagaUsuario($usuario->idusuario);
        $_SESSION['usuario'] = NULL;
      }

      $this->assertEquals(302, $response->getStatusCode());
      $this->assertFlash("sucess", "Usuário atualizado com sucesso.");

      $usuarioFim = $this->dao->recuperaUsuarioPorEmail($this->emailTeste);
      $this->assertNotNull($usuarioFim);
      $this->assertTrue(count($usuarioFim) == 1);
      $usuarioFim = $usuarioFim[0];
      $this->assertEquals($usuarioFim->senha, $senhaAntiga);

      $this->apagaUsuario($usuario->idusuario);
      $this->apagaUsuario($usuarioNovo->idusuario);
      $_SESSION['usuario'] = NULL;
    }

    private function gravaUsuario($email = "teste@teste.com"){
      $usuario = new \stdClass();
      $usuario->idusuario = "";
      $usuario->nome="teste";
      $usuario->email = $email;
      $usuario->senha = "123456";
      $usuario->confirmaSenha = "123456";
      $usuario->idperfil = "1";

      $_SESSION['usuario'] = $usuario;
      $usuario->nome = "teste";
      $response = $this->runApp('POST', '/usuario', $usuario);
      if($response->getStatusCode() == 500){
        print($response->getBody());die;
      }

      $this->assertEquals(302, $response->getStatusCode());
      $this->assertFlash("sucess", "Usuário cadastrado com sucesso.");
    }

    private function apagaUsuario($id){
      $this->assertNotNull($this->dao->getLogger(), 'Nao achou logger');
      $this->dao->apagaUsuario($id);
    }

}
?>

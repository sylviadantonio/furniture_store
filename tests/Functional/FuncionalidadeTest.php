<?php
namespace Tests\Functional;

use sampleStructure\DAO;
use Tests\Utils;

class FuncionalidadeTest extends BaseTestCase
{
  private $dao;
  private $daoPerfil;
  private $helperDao;
  private $userDao;

  public function __construct() {
    $mock = $this->createMock(\Monolog\Logger::class);
    $this->dao = new DAO\FuncionalidadeDAO($mock);
    $this->daoPerfil = new DAO\PerfilDAO($mock);
    $this->userDao = new DAO\UsuarioDAO($mock);
    @session_start();
  }

    public function testPaginaInicial() {
        $response = $this->runApp('GET', '/funcionalidade');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testGravar() {
      print("\n========================================");
      print("\nTeste Funcionalidade Controller gravação");
      print("\n========================================");
      $funcionalidade = new \stdClass();
      $funcionalidade->idfuncionalidade = "";
      $response = $this->runApp('POST', '/funcionalidade', $funcionalidade);
      if($response->getStatusCode() == 500){
        print($response->getBody());
      }
      $this->assertEquals(302, $response->getStatusCode());
      $this->assertFlash("error", "É necessário preencher a URL da funcionalidade.É necessário escolher ao menos um perfil para essa funcionalidade.É necessário preencher o nome da funcionalidade.");

      $funcionalidade->url = "teste";
      $response = $this->runApp('POST', '/funcionalidade', $funcionalidade);
      if($response->getStatusCode() == 500){
        print($response->getBody());die;
      }
      $this->assertEquals(302, $response->getStatusCode());
      $this->assertFlash("error", "É necessário escolher ao menos um perfil para essa funcionalidade.É necessário preencher o nome da funcionalidade.");

      $id = $this->gravaPerfil();
      $funcionalidade->perfilautorizado = array();
      array_push($funcionalidade->perfilautorizado, $id);
      $response = $this->runApp('POST', '/funcionalidade', $funcionalidade);
      if($response->getStatusCode() == 500){
        print($response->getBody());die;
      }
      $this->assertEquals(302, $response->getStatusCode());
      $this->assertFlash("error", "É necessário preencher o nome da funcionalidade.");

      $funcionalidade->nome = "Nome teste";
      $this->populaUsuarioSessao();
      $response = $this->runApp('POST', '/funcionalidade', $funcionalidade);
      if($response->getStatusCode() == 500){
        $this->apagaPerfil($id);
        print($response->getBody());die;
      }

      $this->assertEquals(302, $response->getStatusCode());
      $this->assertFlash("sucess", "Funcionalidade gravada com sucesso.");

      $func = $this->dao->recuperaFuncionalidadePorUrl("teste");
      $this->assertNotNull($func);
      $this->assertTrue(count($func) == 1);
      $func = $func[0];
      $this->assertTrue($func->nome == $funcionalidade->nome);
      //"Nome teste";

      $this->apagaFuncionalidade($func->idfuncionalidade);
      $this->apagaPerfil($id);
    }

    private function apagaFuncionalidade($id){
      $this->assertNotNull($this->dao->getLogger(), 'Nao achou logger');
      $this->dao->apagaPermissaoAcessoPorIdFuncionalidade($id);
      $this->dao->apagaFuncionalidade($id);
    }

    private function gravaPerfil(){
      $this->assertNotNull($this->daoPerfil->getLogger(), 'Nao achou logger');
      return $this->daoPerfil->gravaPerfil("teste controller");
    }

    private function apagaPerfil($id){
      $this->assertNotNull($this->daoPerfil->getLogger(), 'Nao achou logger');
      $this->daoPerfil->apagaPerfil($id);
    }

    private function populaUsuarioSessao() {
      $usuario = $this->userDao->recuperaUsuarioPorId(1);
      $lista = $this->dao->recuperaPermissaoAcessoPorIdPerfil($usuario[0]->idperfil);
      $usuario[0]->urlsPermitidas = $lista;
      $_SESSION['usuario'] = $usuario[0];
    }

}

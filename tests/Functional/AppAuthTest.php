<?php

namespace Tests;

use Tests\Functional;
use sampleStructure\Controller;
use sampleStructure\DAO;
use sampleStructure\Utils;

class AppAuthTest extends Functional\BaseTestCase {
  private $daoFunc;
  private $daoPerfil;
  private $daoUsuario;

  public function __construct() {
    $mock = $this->createMock(\Monolog\Logger::class);
    $this->daoFunc = new DAO\FuncionalidadeDAO($mock);
    $this->daoPerfil = new DAO\PerfilDAO($mock);
    $this->daoUsuario = new DAO\UsuarioDAO($mock);
    session_start();
  }

  public function testValidacaoAcesso(){
    $_SESSION['usuario'] = null;
    $this->validateRoutes = false;

    $response = $this->runApp('GET', '/funcionalidade');
    $this->assertEquals(302, $response->getStatusCode());
    $this->assertContains($response->getHeaders()['Location'][0], "/login");
    $this->assertFlash("error", "É necessário fazer o login pra acessar essa área.");

    $idperfil = $this->gravaPerfil("testeperfil");
    $this->assertNotNull($idperfil, '$idperfil');

    $idfuncionalidade = $this->gravaFuncionalidade("testefunc", "nome", 1);
    $this->assertNotNull($idfuncionalidade, '$idfuncionalidade');

    $idpermissao = $this->gravaPermissaoAcesso($idfuncionalidade, $idperfil);
    $this->assertNotNull($idpermissao, '$idfuncionalidade');

    $idusuario = $this->gravaUsuario($idperfil);
    $this->assertNotNull($idusuario, '$idusuario');

    $usuario = $this->daoUsuario->recuperaUsuarioPorId($idusuario);
    $this->assertNotNull($usuario, '$usuario');
    $usuario = $usuario[0];
    $lista = $this->daoFunc->recuperaPermissaoAcessoPorIdPerfil($usuario->idperfil);
    $this->assertNotNull($lista, '$lista');

    $usuario->urlsPermitidas = $lista;
    $_SESSION['usuario'] = $usuario;

    $response = $this->runApp('GET', '/funcionalidade');
    if($response->getStatusCode() == 500){
      print((string)$response->getBody());
    }
    $this->assertEquals(302, $response->getStatusCode());
    $this->assertContains($response->getHeaders()['Location'][0], "/admin");
    $this->assertFlash("error", "Você não possui autorização para acessar essa área.");

    $idfuncionalidade2 = $this->gravaFuncionalidade("/funcionadlidade");
    $this->assertNotNull($idfuncionalidade2, '$idfuncionalidade2');

    $idpermissao = $this->gravaPermissaoAcesso($idfuncionalidade2, $idperfil);
    $this->assertNotNull($idpermissao, '$idfuncionalidade');

    $lista = $this->daoFunc->recuperaPermissaoAcessoPorIdPerfil($usuario->idperfil);
    $this->assertNotNull($lista, '$lista');

    $usuario->urlsPermitidas = $lista;
    $_SESSION['usuario'] = $usuario;

    $response = $this->runApp('GET', '/funcionalidade');
    $this->assertContains($response->getHeaders()['Location'][0], "/admin");
    $this->assertEquals(302, $response->getStatusCode());
    $this->assertFlash("error", "Você não possui autorização para acessar essa área.");

    $this->apagaUsuario($idusuario);
    $this->apagaFuncionalidade($idfuncionalidade);
    $this->apagaFuncionalidade($idfuncionalidade2);
    $this->apagaPerfil($idperfil);
    $this->validateRoutes = true;
  }

  private function apagaFuncionalidade($id){
    $this->assertNotNull($this->daoFunc->getLogger(), 'Nao achou logger');
    $this->daoFunc->apagaPermissaoAcessoPorIdFuncionalidade($id);
    $this->daoFunc->apagaFuncionalidade($id);
  }

  private function apagaUsuario($id){
    $this->assertNotNull($this->daoUsuario->getLogger(), 'Nao achou logger');
    $this->daoUsuario->apagaUsuario($id);
  }

  private function gravaPerfil($nome = "teste perfil"){
    $this->assertNotNull($this->daoPerfil->getLogger(), 'Nao achou logger');
    return $this->daoPerfil->gravaPerfil($nome);
  }

  private function gravaUsuario($idperfil){
    $this->assertNotNull($this->daoUsuario->getLogger(), 'Nao achou logger');
    $usuario = new \stdClass();
    $usuario->nome = "Teste";
    $usuario->apelido = "Teste";
    $usuario->cep = "Teste";
    $usuario->datanascimento = "1982-03-29";
    $usuario->cpf = "Teste";
    $usuario->endereco = "Teste";
    $usuario->cidade = "rio de janeiro";
    $usuario->uf = "rj";
    $usuario->complemento = "teste";
    $usuario->numero = "1";
    $usuario->bairro = "bairro";
    $usuario->email = "teste@teste123.com";
    $usuario->senha = Utils\Utils::encryptPassword("123456");
    $usuario->datagravacao = date('Y-m-d H:i:s',time());
    $usuario->idperfil = $idperfil;
    return $this->daoUsuario->gravaUsuario($usuario);
  }

  private function gravaFuncionalidade($url = "url funcionalidade", $nome = "nome"){
    $this->assertNotNull($this->daoFunc->getLogger(), 'Nao achou logger funcionalidade');
    return $this->daoFunc->gravaFuncionalidade($url, $nome, 1, 1);
  }

  private function gravaPermissaoAcesso($idfunc, $idperfil){
    $this->assertNotNull($this->daoFunc->getLogger(), 'Nao achou logger funcionalidade');
    return $this->daoFunc->gravaPermissaoAcesso($idfunc, $idperfil);
  }

  private function apagaPerfil($id){
    $this->assertNotNull($this->daoPerfil->getLogger(), 'Nao achou logger');
    $this->daoPerfil->apagaPerfil($id);
  }

}

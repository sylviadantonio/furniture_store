<?php

namespace Tests\Functional;

class HomepageTest extends BaseTestCase
{
    public function testPaginaInicial(){
        $response = $this->runApp('GET', '/');

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('INICIO', (string)$response->getBody());
        $this->assertNotContains('TESTE', (string)$response->getBody());
    }

    public function testMetodoNaoAceito(){
        $response = $this->runApp('POST', '/', ['test']);
        $this->assertEquals(405, $response->getStatusCode());
        $this->assertContains('Method not allowed', (string)$response->getBody());
    }
}

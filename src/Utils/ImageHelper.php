<?php
namespace furnitureStore\Utils;
use furnitureStore\DAO\Base;
use furnitureStore\Service;
use furnitureStore\Utils\Utils;

class ImageHelper {

  private $logger;
  private $service;
  private $THUMBNAIL_SM_MAX = 180;
  private $THUMBNAIL_MD_MAX = 300;
  private $THUMBNAIL_LG_MAX = 500;
  private $IMAGE_MAX = 1900;

  private $IMAGE_INFO = [];
  private $IMAGE_HANDLERS = [];

  public function __construct( $log ) {
    $this -> logger = $log;
    $this -> service = new Service\ProductService($this -> logger);
    $this -> IMAGE_INFO = [
        180 => [
          'max' => '180',
          'min' => '120',
          'folder' => Globals::$FOLDER_THUMBS . Globals::$FOLDER_180,
          'sufix' => '_180x120'
        ],
        300 => [
          'max' => '300',
          'min' => '200',
          'folder' => Globals::$FOLDER_THUMBS . Globals::$FOLDER_300,
          'sufix' => '_300x200'
        ],
        500 => [
          'max' => '500',
          'min' => '333',
          'folder' => Globals::$FOLDER_THUMBS . Globals::$FOLDER_500,
          'sufix' => '_500x333'
        ],
        1900 => [
          'max' => '1900',
          'min' => '1266',
          'folder' => Globals::$FOLDER_IMG_ORIGINAL,
          'sufix' => ''
        ]
      ];

      $this -> IMAGE_HANDLERS = [
        IMAGETYPE_JPEG => [
            'load' => 'imagecreatefromjpeg',
            'save' => 'imagejpeg',
            'quality' => 100,
            'type' => '.jpeg'
        ],
        IMAGETYPE_PNG => [
            'load' => 'imagecreatefrompng',
            'save' => 'imagepng',
            'quality' => 0,
            'type' => '.png'
        ],
        IMAGETYPE_WEBP => [
            'load' => 'imagecreatefromwebp',
            'save' => 'imagewebp',
            'quality' => 100,
            'type' => '.webp'
        ]
      ];
  }

  public function getLogger() {
    return $this->logger;
  }



/**
 * @param $src - a valid file location
 * @param $targetMaxSize - one of the options above
 */
public function createThumbnail($src, $nameDest, $targetMaxSize) {

    // 1. Load the image from the given $src
    // - see if the file actually exists
    // - check if it's of a valid image type
    // - load the image resource

    // get the type of the image
    // we need the type to determine the correct loader
    $type = exif_imagetype($src);

    // if no valid type or no handler found -> exit
    if (!$type || !$this -> IMAGE_HANDLERS[$type]) {
        return null;
    }

    // load the image with the correct loader
    $image = call_user_func($this -> IMAGE_HANDLERS[$type]['load'], $src);

    // no image found at supplied location -> exit
    if (!$image) {
        return null;
    }

    // 2. Create a thumbnail and resize the lo$this -> aded $image
    // - get the image dimensions
    // - define the output size appropriately
    // - create a thumbnail based on that size
    // - set alpha transparency for GIFs and PNGs
    // - draw the final thumbnail

    // get original image width and height
    $width = imagesx($image);
    $height = imagesy($image);
    $isLandscape = $width > $height;
    $info = $this -> IMAGE_INFO[$targetMaxSize];
    $targetWidth = $isLandscape ? $info['max'] : $info['min'];
    $targetHeight = $isLandscape ? $info['min'] : $info['max'];

    /*
    // maintain aspect ratio when no height set
    if ($targetHeight == null) {

        // get width to height ratio
        $ratio = $width / $height;

        // if is portrait
        // use ratio to scale height to fit in square
        if ($width > $height) {
            $targetHeight = floor($targetWidth / $ratio);
        }
        // if is landscape
        // use ratio to scale width to fit in square
        else {
            $targetHeight = $targetWidth;
            $targetWidth = floor($targetWidth * $ratio);
        }
    }*/




    // create duplicate image based on calculated target size
    $thumbnail = imagecreatetruecolor($targetWidth, $targetHeight);

    // set transparency options for GIFs and PNGs
    if ($type == IMAGETYPE_GIF || $type == IMAGETYPE_PNG) {

        // make image transparent
        imagecolortransparent(
            $thumbnail,
            imagecolorallocate($thumbnail, 0, 0, 0)
        );

        // additional settings for PNGs
        if ($type == IMAGETYPE_PNG) {
            imagealphablending($thumbnail, false);
            imagesavealpha($thumbnail, true);
        }
    }

    // copy entire source image to duplicate image and resize
    imagecopyresampled(
        $thumbnail,
        $image,
        0, 0, 0, 0,
        $targetWidth, $targetHeight,
        $width, $height
    );


    // 3. Save the $thumbnail to disk
    // - call the correct save method
    // - set the correct quality level

    // save the duplicate version of the image to disk
    if( call_user_func(
        $this -> IMAGE_HANDLERS[$type]['save'],
        $thumbnail,
        Globals::$IMG_UPLOAD_SERVER_PATH . Globals::$PASTA_IMG_UPLOAD . $info['folder'] . "/" . $nameDest . $info['sufix'] . $this -> IMAGE_HANDLERS[$type]['type'],
        $this -> IMAGE_HANDLERS[$type]['quality']
    ) ) {
      return ['name' =>  $nameDest . $info['sufix'] . $this -> IMAGE_HANDLERS[$type]['type'],
              'folder' => $info['folder'] ];
    } else {
      return [];
    }
}

  /**
  * Method that create the img variation,
  * and if successfully inserted, insert into the database else stops right there.
  **/
  public function addInsertImg($src, $name, $idProduct, $mainPic, $title) {
    // cria a primeira imagem
    $result = $this -> createThumbnail($src, $name, $this -> IMAGE_MAX );
    if( !empty( $result ) ) {
      $newid = $this -> service -> insertProductPicture( $idProduct, $result['name'], $result['folder'], $mainPic, $title);
      if( $newid != null ) {

        $result = $this -> createThumbnail($src, $name, $this -> THUMBNAIL_LG_MAX );
        if( !empty( $result ) ) {
          $newid = $this -> service -> insertProductPicture( $idProduct, $result['name'], $result['folder'], $mainPic, $title);
          if( $newid != null ) {

            $result = $this -> createThumbnail($src, $name, $this -> THUMBNAIL_MD_MAX );
            if( !empty( $result ) ) {
                $newid = $this -> service -> insertProductPicture( $idProduct, $result['name'], $result['folder'], $mainPic, $title);
                if( $newid != null )  {

                  $result = $this -> createThumbnail($src, $name, $this -> THUMBNAIL_SM_MAX );
                  if( !empty( $result ) ) {
                    $newid = $this -> service -> insertProductPicture( $idProduct, $result['name'], $result['folder'], $mainPic, $title);
                    if( $newid != null ) {
                      return true;
                    } else {
                      $this -> logger -> error("There was an error on inserting the image : " . $result['name'] . " in the db." );
                    }
                  } else {
                    $this -> logger -> error( "There was an error to creat the thumbnail size 180x120." );
                  }
                } else {
                  $this -> logger -> error("There was an error on inserting the image : " . $result['name'] . " in the db." );
                }
            } else {
              $this -> logger -> error( "There was an error to creat the thumbnail size 300x200." );
            }
          } else {
              $this -> logger -> error("There was an error on inserting the image : " . $result['name'] . " in the db." );
          }
        } else {
            $this -> logger -> error("There was an error to create the thumbnail size 500x333." );
        }
      } else {
          $this -> logger -> error("There was an error on inserUtils\ting the image : " . $result['name'] . " in the db." );
      }
    } else {
      $this -> logger -> error("There was an error to creat the image size 1900x1266.");
    }

    return false;
  }

  public function treatImages($files, $idProduct, $mainTitle, $listTitles) {
    $original = $files['main_pic'] ?? [];
    $success = true;

    //add the the main pic
    if( !empty( $original ) ) {
      $originalImgName = $this -> generateImgName( $original['name'] );
      if( !$this -> addInsertImg( $original['tmp_name'], $originalImgName, $idProduct, 1, $mainTitle) ) {
        $this -> logger -> error("There was an error to creat the main image.");
        $success = false;
      }
    }

    if(isset($files['gallery_pictures']) && !empty($files['gallery_pictures'])) {
      //check if the main pic was already inserted in the galery
      // if so, get the index to not insert again
      $repeated = "";
      if( !empty( $original ) ) {
        foreach ( $files['gallery_pictures']['name'] as $x => $galPic ) {
          if( $galPic == $original[ 'name' ] &&
              $files[ 'gallery_pictures' ][ 'size' ][ $x ] == $original['size']) {
                $repeated = $x;
            break;
          }
        }
      }
      $fileList = $files[ 'gallery_pictures' ]['name'];
      //generate all the images
      for( $x = 0; $x < count( $fileList ); $x++ ) {
        if($x != $repeated) {
          $imgName = $this -> generateImgName( $fileList[ $x ] );
          if(!$this -> addInsertImg( $files['gallery_pictures']['tmp_name'][$x], $imgName, $idProduct, 0, $listTitles[ $x ])) {
              return false;
          }
        }
      }
      return true;
    }
  }

  private function generateImgName($originalName) {
    $type = explode('.', $originalName);
    $type = $type[ ( count( $type ) -1 ) ];
    $imgName = str_replace(".", "", uniqid(Globals::$PREFIXO_IMG_UPLOAD . date("Y-m-d-H-i-s") .".". $type, true));
    return $imgName;
  }
}

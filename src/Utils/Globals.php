<?php
namespace furnitureStore\Utils;

class Globals {

  //Global
  public static $DATA_FORTMATO_DB_PADRAO = "Y-m-d H:i:s";
  public static $TAMANHO_MAX_POST = 1088608;

  public static $LOCATION = "Location";
  public static $PUBLIC_FOLDER_PATH = "/var/opt/Projects/furniture-store/public";
  public static $FOLDER_QR = "/QR/";
  public static $FOLDER_PICS = "pics/";

  //usuario
  public static $USUARIO_SESSAO = 'usuario';
  public static $USUARIO_CADASTRO = 'usuarioCad';
  public static $USUARIO_TEMPLATE = 'usuario/usuario.html';
  public static $USUARIO_URL = '/user';
  public static $USUARIOS = 'usuarios';
  //perfil
  public static $PERFIL = 'perfil';
  public static $PERFIS = 'perfis';
  public static $PERFIL_URL = '/profile';
  public static $PERFIL_TEMPLATE = 'usuario/perfil.html';

  //login
  public static $LOGIN_URL = '/login';
  public static $LOGIN_TEMPLATE = 'usuario/login.html';
  //admin
  public static $ADMIN_INICIO_URL = '/admin';
  public static $ADMIN_INICIO_TEMPLATE = 'inicioadmin.html';

  //funcionalidade
  public static $FUNCIONALIDADE = 'funcionalidade';
  public static $FUNCIONALIDADES = 'funcionalidades';
  public static $FUNCIONALIDADE_TEMPLATE = 'funcionalidade.html';
  public static $FUNCIONALIDADE_URL = '/functionality';

  //client add
  public static $CLIENT = 'client';
  public static $CLIENT_FORM = 'clientform';
  public static $CLIENT_FORM_TEMPLATE = 'client/client-form.html';
  public static $CLIENT_FORM_URL = '/client/add';

  //client list
  public static $CLIENT_LIST = 'clientlist';
  public static $CLIENT_LIST_TEMPLATE = 'client/clients-list.html';
  public static $CLIENT_LIST_URL = '/client/list';
  public static $CLIENT_EDIT_URL = '/client/edit/';

  //--------> PRODUCT
  //Category
  public static $CATEGORY = 'category';
  public static $CATEGORIES = 'categories';
  public static $LIST_CATEGORIES = 'listCategories';
  public static $CATEGORY_URL = '/category';
  public static $CATEGORY_TEMPLATE = 'product/category.html';

  //Tag
  public static $TAG = 'tag';
  public static $TAGS = 'tags';
  public static $TAG_URL = '/tag';
  public static $TAG_TEMPLATE = 'product/tag.html';

  //Product add
  public static $PRODUCT = 'product';
  public static $PRODUCTS = 'products';
  public static $PRODUCT_FORM_URL = '/product/add';
  public static $PRODUCT_FORM_TEMPLATE = 'product/product-form.html';

  //Product
  public static $PRODUCT_LIST = 'productlist';
  public static $PRODUCT_LIST_TEMPLATE = 'product/products-list.html';
  public static $PRODUCT_LIST_URL = '/product/list';
  public static $PRODUCT_EDIT_URL = '/product/edit/';


  //imagens
  public static $PREFIXO_IMG_UPLOAD = 'kg_';
  public static $PASTA_IMG_UPLOAD = '/pics';
  public static $FOLDER_IMG_ORIGINAL = '/original';
  public static $FOLDER_THUMBS = '/thumbnails';
  public static $FOLDER_180 = '/180x120';
  public static $FOLDER_300 = '/300x200';
  public static $FOLDER_500 = '/500x333';

  public static $IMG_UPLOAD_SERVER_PATH ='/var/opt/Projects/furniture-store/public';
  public static $IMG_UPLOAD_SERVER_PATH_PRD = '***CAMINHO_PRODUCAO***';
}
?>

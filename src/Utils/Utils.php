<?php
namespace furnitureStore\Utils;

class Utils {

  public static function encryptPassword($string) {
    return password_hash($string, PASSWORD_BCRYPT);
  }

  public static function createFriendlyURL($string) {
    $string = strtolower(utf8_decode($string)); $i=1;
    $string = strtr($string, utf8_decode('àáâãäåæçèéêëìíîïñòóôõöøùúûýýÿ'), 'aaaaaaaceeeeiiiinoooooouuuyyy');
    $string = preg_replace("/([^a-z0-9])/",'-',utf8_encode($string));
    while($i>0) {$string = str_replace('--','-',$string,$i);};
    if (substr($string, -1) == '-') {$string = substr($string, 0, -1);};
    return $string;
  }

  public static function mask($val, $mask) {
   $maskared = '';
   $k = 0;
   for($i = 0; $i<=strlen($mask)-1; $i++) {
     if($mask[$i] == '#'){
       if(isset($val[$k])){
         $maskared .= $val[$k++];
       }
     } else {
       if(isset($mask[$i])){
         $maskared .= $mask[$i];
       }
     }
   }
   return $maskared;
  }

  public static function limpaPontosTracos($string) {
    return preg_replace('/[\.\/\-]/','', $string);
  }
  public static function validaEmail($email){
    return filter_var($email, FILTER_VALIDATE_EMAIL);
  }

  public static function validaCpf($cpf) {
    //Etapa 1: Cria um array com apenas os digitos numéricos, isso permite receber o cpf em diferentes formatos como "000.000.000-00", "00000000000", "000 000 000 00" etc...
			$j=0;
			for($i=0; $i<(strlen($cpf)); $i++)
				{
					if(is_numeric($cpf[$i]))
						{
							$num[$j]=$cpf[$i];
							$j++;
						}
				}
			//Etapa 2: Conta os dígitos, um cpf válido possui 11 dígitos numéricos.
			if(count($num)!=11)
				{
					$isCpfValid=false;
				}
			//Etapa 3: Combinações como 00000000000 e 22222222222 embora não sejam cpfs reais resultariam em cpfs válidos após o calculo dos dígitos verificares e por isso precisam ser filtradas nesta parte.
			else
				{
					for($i=0; $i<10; $i++)
						{
							if ($num[0]==$i && $num[1]==$i && $num[2]==$i && $num[3]==$i && $num[4]==$i && $num[5]==$i && $num[6]==$i && $num[7]==$i && $num[8]==$i)
								{
									$isCpfValid=false;
									break;
								}
						}
				}
			//Etapa 4: Calcula e compara o primeiro dígito verificador.
			if(!isset($isCpfValid))
				{
					$j=10;
					for($i=0; $i<9; $i++)
						{
							$multiplica[$i]=$num[$i]*$j;
							$j--;
						}
					$soma = array_sum($multiplica);
					$resto = $soma%11;
					if($resto<2)
						{
							$dg=0;
						}
					else
						{
							$dg=11-$resto;
						}
					if($dg!=$num[9])
						{
							$isCpfValid=false;
						}
				}
			//Etapa 5: Calcula e compara o segundo dígito verificador.
			if(!isset($isCpfValid))
				{
					$j=11;
					for($i=0; $i<10; $i++)
						{
							$multiplica[$i]=$num[$i]*$j;
							$j--;
						}
					$soma = array_sum($multiplica);
					$resto = $soma%11;
					if($resto<2)
						{
							$dg=0;
						}
					else
						{
							$dg=11-$resto;
						}
					if($dg!=$num[10])
						{
							$isCpfValid=false;
						}
					else
						{
							$isCpfValid=true;
						}
				}
			//Trecho usado para depurar erros.
			//Etapa 6: Retorna o Resultado em um valor booleano.
			return $isCpfValid;
  }

  public static function arrumaDataBancoParaExibicao($data){
      if($data != ""){
        $dataT = explode("-", $data);
        return $dataT[2]."/".$dataT[1]."/".$dataT[0];
      } else {
        return "";
      }
  }

  public static function validaCnpj($cnpj) {
  	$cnpj = preg_replace('/[^0-9]/', '', (string) $cnpj);
  	// Valida tamanho
  	if (strlen($cnpj) != 14){
      return false;
    }

  	// Valida primeiro dígito verificador
  	for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++)
  	{
  		$soma += $cnpj{$i} * $j;
  		$j = ($j == 2) ? 9 : $j - 1;
  	}
  	$resto = $soma % 11;
  	if ($cnpj{12} != ($resto < 2 ? 0 : 11 - $resto)){
      return false;
    }
  	// Valida segundo dígito verificador
  	for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++)
  	{
  		$soma += $cnpj{$i} * $j;
  		$j = ($j == 2) ? 9 : $j - 1;
  	}
  	$resto = $soma % 11;

  	return $cnpj{13} == ($resto < 2 ? 0 : 11 - $resto);
  }


  public static function generatePassword($qtyCaraceters = 8) {
    //Letras minúsculas embaralhadas
    $smallLetters = str_shuffle('abcdefghijklmnopqrstuvwxyz');

    //Letras maiúsculas embaralhadas
    $capitalLetters = str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ');

    //Números aleatórios
    $numbers = (((date('Ymd') / 12) * 24) + mt_rand(800, 9999));
    $numbers .= 1234567890;

    //Caracteres Especiais
    $specialCharacters = str_shuffle('!@#$%*-');

    //Junta tudo
    $characters = $capitalLetters.$smallLetters.$numbers.$specialCharacters;

    //Embaralha e pega apenas a quantidade de caracteres informada no parâmetro
    return substr(str_shuffle($characters), 0, $qtyCaraceters);
  }
}
?>

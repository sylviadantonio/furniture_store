<?php

use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;

return function (App $app) {
    /* rotas públicas*/
    $app->get('/', "furnitureStore\Controller\InicioController:inicio");
    $app->get('/admin', "furnitureStore\Controller\LoginController:adminInicio");

    $app->get('/user', "furnitureStore\Controller\UserController:inicio");
    $app->post('/user', "furnitureStore\Controller\UserController:gravar");
    $app->get('/user/{id}', "furnitureStore\Controller\UserController:edita");
    $app->post('/user/{id}', "furnitureStore\Controller\UserController:apaga");
    $app->post('/user/disactivate/{id}', "furnitureStore\Controller\UserController:inativa");
    $app->post('/user/activate/{id}', "furnitureStore\Controller\UserController:ativa");

    $app->get('/login', "furnitureStore\Controller\LoginController:inicio");
    $app->post('/login', "furnitureStore\Controller\LoginController:entrar");
    $app->get('/logout', "furnitureStore\Controller\LoginController:sair");

    $app->get('/functionality', "furnitureStore\Controller\FuncionalidadeController:inicio");
    $app->get('/functionality/{id}', "furnitureStore\Controller\FuncionalidadeController:edita");
    $app->post('/functionality', "furnitureStore\Controller\FuncionalidadeController:gravar");
    $app->post('/functionality/{id}', "furnitureStore\Controller\FuncionalidadeController:apaga");

    $app->get('/profile', "furnitureStore\Controller\PerfilController:inicio");
    $app->get('/profile/{id}', "furnitureStore\Controller\PerfilController:edita");
    $app->post('/profile', "furnitureStore\Controller\PerfilController:gravar");
    $app->post('/profile/{id}', "furnitureStore\Controller\PerfilController:apaga");

    $app->get('/client/add', "furnitureStore\Controller\Client\ClientFormController:start");
    $app->post('/client/add', "furnitureStore\Controller\Client\ClientFormController:insert");
    $app->get('/client/list', "furnitureStore\Controller\Client\ClientListController:start");
    $app->get('/client/edit/{id}', "furnitureStore\Controller\Client\ClientFormController:edit");
    $app->post('/client/delete/{id}', "furnitureStore\Controller\Client\ClientListController:delete");
    $app->get('/client/{id}', "furnitureStore\Controller\Client\ClientListController:getUserInfo");

    $app->get('/product/add', "furnitureStore\Controller\Product\ProductFormController:start");
    $app->post('/product/add', "furnitureStore\Controller\Product\ProductFormController:insert");
    $app->get('/product/qr/{text}', "furnitureStore\Controller\Product\ProductFormController:getQRCode");
    $app->get('/product/check-friedly-url/{text}', "furnitureStore\Controller\Product\ProductFormController:checkFriendlyUrl");


    $app->get('/tag', "furnitureStore\Controller\Product\TagController:start");
    $app->post('/tag', "furnitureStore\Controller\Product\TagController:insert");
    $app->post('/tag-async', "furnitureStore\Controller\Product\TagController:insertAsynchronous");
    $app->get('/tag/{id}', "furnitureStore\Controller\Product\TagController:edit");
    $app->post('/tag/{id}', "furnitureStore\Controller\Product\TagController:delete");
    $app->get('/tag/search/{text}', "furnitureStore\Controller\Product\TagController:search");

    $app->get('/category', "furnitureStore\Controller\Product\CategoryController:start");
    $app->post('/category', "furnitureStore\Controller\Product\CategoryController:insert");
    $app->get('/category/{id}', "furnitureStore\Controller\Product\CategoryController:edit");
    $app->post('/category/{id}', "furnitureStore\Controller\Product\CategoryController:delete");

};

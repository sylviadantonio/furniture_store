<?php
namespace furnitureStore\Service;
use furnitureStore\DAO;

class CategoryService {

  private $logger;
  private $dao;

  public function __construct($log) {
     $this->logger = $log;
     $this->dao = new DAO\CategoryDAO($this->logger);
  }

  public function getLogger() {
    return $this->logger;
  }

  public function insertCategory( $category ) {
    return $this -> dao -> insertCategory( $category );
  }

  public function updateCategory( $category ) {
    return $this -> dao -> updateCategory( $category );
  }

  public function getCategoryByName( $name ) {
    return $this -> dao -> getCategoryByName( $name );
  }

  public function findAllCategories() {
    return $this -> dao -> findAllCategories();
  }

  public function getCategoryById( $id ) {
    return $this -> dao -> getCategoryById( $id );
  }

  public function deleteCategory( $id ) {
    return $this -> dao -> deleteCategory( $id );
  }

  public function orderCategory( $categoryList ) {
    $rootList = [];
    $childrensList = [];
    foreach ( $categoryList as $i => $cat ) {
      $cat -> children = [];
      if( $cat -> id_cat_parent == null || $cat -> id_cat_parent == "" ) {
        $rootList[] = $cat;
        unset( $categoryList[$i] );
      }
    }
    foreach ($categoryList as $cat) {
        $rootList = $this -> findParent($rootList, $cat);
    }
    return $rootList;
  }

  private function findParent( $parentList, $child ) {
      foreach( $parentList as $parent ) {
        if( $parent -> id_category ==  $child -> id_cat_parent) {
          array_push( $parent -> children, $child );
        } else {
          if(!empty( $parent -> children ) ) {
              $parent -> children = $this->findParent($parent -> children, $child);
          }
        }
      }
    return $parentList;
  }
}

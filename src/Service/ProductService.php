<?php
namespace furnitureStore\Service;
use furnitureStore\DAO;
use furnitureStore\Utils\Utils;

class ProductService {

  private $logger;
  private $dao;

  public function __construct( $log ) {
    $this -> dao = new DAO\ProductDAO( $this -> logger );
    $this -> logger = $log;
  }

  public function getLogger() {
    return $this->logger;
  }

  public function prepareFriendyUrl( $title, $atempts ) {
    $url = Utils::createFriendlyURL( $title );

    if(!$this -> dao -> checkFriendlyURL($url)) {
      return $url;
    } else {
      $arrTemp = explode("-", $url);
      $n = $atempts > 1 ? (count($arrTemp) - 1) : count($arrTemp);

      $arrTemp[$n] = "".$atempts;
      $urlFinal = $arrTemp[0];
      for($x = 1; $x < count($arrTemp); $x++) {
        $urlFinal .="-" .$arrTemp[$x];
      }
      return $this -> prepareFriendyUrl($urlFinal, $atempts+1);
    }
  }

  public function insertProductPicture( $idProduct, $imgName, $folder, $isMain, $title ){
    return $this -> dao ->insertProductPicture( $idProduct, $imgName, $folder, $isMain, $title );
  }

}

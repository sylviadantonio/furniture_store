<?php
namespace furnitureStore\DAO;
use furnitureStore\DAO\Base;
use furnitureStore\Utils\Utils;

class ClientDAO extends BaseDAO {
  private $logger;

	public function __construct($log) {
     $this->logger = $log;
  }

  public function getLogger() {
  	return $this->logger;
	}

  public function insertClient($client) {
    $sql = "INSERT INTO client (
              name, phone_1, phone_2, ext, id_address, status, id_user) VALUES
                (:name, :phone_1, :phone_2, :ext, :id_address, :status, :id_user)";

    return $this->insertDB($sql, array(
                ':name' => $client->name,
                ':phone_1' => $client->phone_1,
                ':phone_2' => $client->phone_2,
                ':ext' => $client->ext,
                ':id_address' => $client->id_address,
                ':status' => $client->status,
                ':id_user' => $client->id_user), "INSERT ADDRESS", false);
    }

    public function getCompleteClientById( $id ) {
      $sql = "SELECT c.*, u.*, a.* FROM client c
        JOIN user u ON u.id_user = c.id_user
        JOIN address a ON a.id_address = c.id_address WHERE c.id_client = :id";

      return $this -> selectDB( $sql, [ ':id' => $id ],
    			"furnitureStore\Model\Modelo", "RETRIEVE COMPLETE INFO OF CLIENT BY ID: " . $id, false );

    }

    public function updateClient($client) {

      $sql = "UPDATE client SET
                name = :name, phone_1 = :phone_1, phone_2 = :phone_2,
                ext = :ext, id_address = :id_address,
                status = :status, id_user = :id_user
        WHERE id_client = :id";
        $id = $this -> updateDB( $sql, array(
            ':name' => $client->name,
            ':phone_1' => $client->phone_1,
            ':phone_2' => $client->phone_2,
            ':ext' => $client->ext,
            ':id_address' => $client->id_address,
            ':status' => $client->status,
            ':id_user' => $client->id_user,
            ':id' => $client->id_client), "UPDATE CLIENT", false);

            if($id != "") {
                return true;
            }
            return false;
    }

    public function retrieveClientByUserId($id_user) {
      $sql = "SELECT * FROM client WHERE id_user = :id_user";
      return $this -> selectDB( $sql, [ ':id_user' => $id_user ],
    			"furnitureStore\Model\Modelo", "RETRIEVE CLIENT INFO BY ID_USER: " . $id, false );
    }

    public function getAllClientsComplete() {
      $sql = "SELECT c.*, a.*, u.* FROM client c
	               JOIN user u ON c.id_user = u.id_user
                JOIN address a ON c.id_address = a.id_address WHERE c.status = 1";
    return $this -> selectDB( $sql, [ ],
              			"furnitureStore\Model\Modelo", "RETRIEVE ALL CLIENTS WITH ITS INFO", false );
    }

    public function getAllClientsPartialInfo(){
      $sql = "SELECT c.id_client, c.name, c.phone_1, c.phone_2, c.ext, u.username, u.email  FROM client c
	               JOIN user u ON c.id_user = u.id_user WHERE c.status = 1";
    return $this -> selectDB( $sql, [ ],
              			"furnitureStore\Model\Modelo", "RETRIEVE ALL CLIENTS WITH ITS BASIC INFO", false );
    }

    public function getClient( $id_client ) {

      $sql = " SELECT * FROM client c WHERE c.id_client = :id_client";
      return $this -> selectDB( $sql, [ ':id_client' => $id_client ],
                			"furnitureStore\Model\Modelo", "RETRIEVE CLIENT WITH ITS BASIC INFO", false );
    }

    public function inactivateClientAndUser( $id_client, $id_user ){
      $sql1 = "UPDATE client c SET c.status = 0 WHERE c.id_client = :id_client ";
      $id1 = $this->updateDB($sql1, [ ':id_client' => $id_client ], "UPDATE CLIENT TO INACTIVE ", false );
      //var_dump($id1);die;
      if($id1 != "") {
        $sql2 = "UPDATE user u SET u.status = 0 WHERE u.id_user = :id_user ";
        
        $id2 = $this->updateDB($sql2, [ ':id_user' => $id_user ], "UPDATE USER TO INACTIVE ", false );
        if($id2 != "") {
            return true;
        }
      }
      return false;
    }
}

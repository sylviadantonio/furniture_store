<?php
namespace furnitureStore\DAO;
use furnitureStore\DAO\Base;

class ErrorDAO extends BaseDAO {
	private $logger;

	public function __construct($log) {
         $this->logger = $log;
    }

    public function getLogger() {
    	return $this->logger;
 	 }

	 public function recuperaTodosErros() {
		 $sql = "SELECT * " .
				 "FROM logerror ORDER BY datetime";
		 return $this->selectDB($sql, null, "furnitureStore\Model\Modelo", "RECOVER ALL ERRORS BY DATE");
	 }

	 public function recuperaErroPoraction($nomeTag) {
		 $sql = "SELECT * " .
				 "FROM logerror t " .
				 "WHERE
				 t.action = :action";
		 return $this->selectDB($sql, array(':action' => $nomeTag),
			 "furnitureStore\Model\Modelo", "RECOVER ERRORS BY ACTION : " . $nomeTag, false);
	 }

	 public function recuperaErroPorId($id) {
		 $sql = "SELECT * " .
				 "FROM logerror t " .
				 "WHERE
				 t.id = :id";
		 return $this->selectDB($sql, array(':id' => $id),
			 "furnitureStore\Model\Modelo", "RECOVER ERROR BY ID: " . $id, false);
	 }

    public function gravaErro ($erro) {
      $sqlGravaErro =
        "INSERT INTO " .
            " logerror (" .
            " sqlerror, datetime, action, resolved, " .
            " message, datelastchange) ".
        " VALUES (" .
            " :sql, :data, :action, 'N', " .
            " :msg, :ultalt)";

          return $this->insertDB($sqlGravaErro,
          	array(':sql' => $erro->sqlerror, ':data' => $this->getHoje(),
				':action' => $erro->action, ':msg' => $erro->message,
				':ultalt' => $this->getHoje()), "INSERT ERROR");

    }

		public function apagaErro($id) {
      $sql = "DELETE " .
          "FROM logerror " .
          "WHERE id = :id ";
      return $this->deleteDB($sql, array(':id' => $id), "DELETE ERROR ID: $id ", false);
    }

    public function recuperaErro() {
        $sqlRecuperaErro =
        "SELECT " .
            " id, action, sqlerror, datetime, resolved, message, datelastchange " .
        "FROM " .
            "logerror " .
        "WHERE " .
            "resolved = 'N' ".
        "ORDER BY datetime DESC";

        return $this->executaQueryComRetorno($sqlRecuperaErro, "RECOVER ERRORS");
    }

    public function fechaErro ($id) {
        $sqlFechaErro =
        "UPDATE logerror SET resolved = 'Y' WHERE id = $id ";

        return $this->executaQuery($sqlFechaErro, "CLOSE ERROR");
    }

}

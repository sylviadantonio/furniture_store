<?php
namespace furnitureStore\DAO;
use furnitureStore\DAO\Base;

class AddressDAO extends BaseDAO {
  private $logger;

	public function __construct($log) {
     $this->logger = $log;
  }

  public function getLogger() {
  	return $this->logger;
	}

  public function insertAddress($address) {
    $sql = "INSERT INTO address (
          title, street,number,complement,postal_code,city,province)
            VALUES(:title, :street, :number, :complement, :postal_code, :city, :province)";

    return $this->insertDB($sql, array(
                    ':title' => $address->title,
                    ':street' => $address->street,
                    ':number' => $address->number,
                    ':complement' => $address->complement,
                    ':postal_code' => $address->postal_code,
                    ':city' => $address->city,
                    ':province' => strtoupper($address->province)), "INSERT ADDRESS", false);
  }

  public function updateAddress( $address ) {
    $sql = "UPDATE address SET
                title = :title, street = :street, number = :number,
                complement = :complement, postal_code = :postal_code,
                city = :city, province = :province WHERE id_address = :id";
      $id = $this -> updateDB( $sql, array(
                      ':id' => $address->id_address,
                      ':title' => $address->title,
                      ':street' => $address->street,
                      ':number' => $address->number,
                      ':complement' => $address->complement,
                      ':postal_code' => $address->postal_code,
                      ':city' => $address->city,
                      ':province' => strtoupper($address->province)), "UPDATE ADDRESS", false);

      if($id != "") {
          return true;
      }
      return false;
  }

}

<?php
namespace furnitureStore\DAO;

class ArquivoDAO {
  private $logger;

	public function __construct($log) {
     $this->logger = $log;
  }

  protected function moverArquivoTemp($caminhoTmp, $novoNome){
    if (strpos($_SERVER['HTTP_HOST'], 'localhost') !== FALSE){
      return move_uploaded_file($caminhoTmp, "/mnt/compartilhado/arquivos/" . $novoNome);
    } else {
      return move_uploaded_file($caminhoTmp, "/public_html/fotos/" . $novoNome);
    }
  }

  protected function recuperaExtensao($arquivo){
  $ex = "";
  if($arquivo["error"] == 0){
    if($arquivo["type"] == "application/msword"){ $ex = ".doc"; } else
    if($arquivo["type"] == "application/pdf"){ $ex = ".pdf"; } else
    if($arquivo["type"] == "application/vnd.ms-excel"){ $ex = ".xls"; } else
    if($arquivo["type"] == "application/vnd.ms-powerpoint"){ $ex = ".pps"; } else
    if($arquivo["type"] == "application/zip"){ $ex = ".zip"; } else
    if($arquivo["type"] == "image/bmp"){ $ex = ".bmp"; } else
    if($arquivo["type"] == "image/gif"){ $ex = ".gif"; } else
    if($arquivo["type"] == "image/jpeg"){ $ex = ".jpg"; } else
    if($arquivo["type"] == "text/html"){ $ex = ".html"; } else
    if($arquivo["type"] == "application/vnd.openxmlformats-officedocument.wordprocessingml.document"){ $ex = ".docx"; } else
    if($arquivo["type"] == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"){ $ex = ".xlsx"; } else
    if($arquivo["type"] == "application/vnd.openxmlformats-officedocument.presentationml.slideshow"){ $ex = ".ppsx"; } else
    if($arquivo["type"] == "text/plain"){ $ex = ".txt"; }
    if ($ex != "" && ($ex == ".doc" || $ex == ".pdf" || $ex == ".xls"
      || $ex == ".pps" || $ex == ".zip" || $ex == ".bmp" || $ex == ".gif"
      || $ex == ".jpg" || $ex == ".docx" || $ex == ".xlsx" || $ex == ".ppsx"
      || $ex == ".txt" || $ex == ".html")){
      return $ex;
    } else {
      return "";
    }
  }
}

protected function geraNomeImagem($ex){
  if($ex != ""){
    return md5(uniqid(rand(), true)).$ex;
  }
  return "";
}

}

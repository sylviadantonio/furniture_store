<?php
namespace furnitureStore\DAO;
use furnitureStore\DAO\Base;
use furnitureStore\Utils\Utils;

class CategoryDAO extends BaseDAO {
  private $logger;

	public function __construct($log) {
     $this->logger = $log;
  }

  public function getLogger() {
  	return $this->logger;
	}
  public function findAllCategories() {
    $sql = "SELECT c.id_category, c.name, c.id_cat_parent, p.name as parent
              FROM category c
                LEFT JOIN category p ON c.id_cat_parent = p.id_category;";

    return $this -> selectDB( $sql, [], "furnitureStore\Model\Modelo", "GET ALL CATEGORIES", false);
  }

  public function insertCategory( $category ) {

    $sql = "INSERT INTO category (name, id_cat_parent)
              VALUES (:name, :id_cat_parent)";

    return $this -> insertDB( $sql, [ ':name' => $category -> name,
                          ':id_cat_parent' => $category -> id_cat_parent ], "INSERT NEW CATEGORY", false );
  }

  public function getCategoryByName( $name ){
    $sql = " SELECT * FROM category WHERE name = :name ";

    return $this -> selectDB ( $sql, [ ':name' => $name ], "furnitureStore\Model\Modelo", "GET CATEGORY BY NAME: " . $name, false);
  }

  public function getCategoryById( $id ) {
    $sql = " SELECT * FROM category WHERE id_category = :id ";

    return $this -> selectDB( $sql, [ ':id' => $id ], "furnitureStore\Model\Modelo", "GET CATEGORY BY ID: " . $id, false);
  }

  public function updateCategory( $category ) {

    $sql = "UPDATE category SET name = :name, id_cat_parent = :id_cat_parent WHERE id_category = :id";
    $id = $this -> updateDB( $sql,
        [ ':name' => $category -> name,
          ':id_cat_parent' => $category -> id_cat_parent,
          ':id' => $category -> id_category ], "UPDATE CATEGORY ID: " . $category -> id_cat_parent, false );

    if($id != ""){
      return true;
    }
    return false;
  }

  public function deleteCategory( $id ) {
    $sql = "DELETE FROM category WHERE id_category = :id";
    return $this->deleteDB($sql, array(':id' => $id), "DELETE CATEGORY BY ID: $id ", false);
  }
}

<?php
namespace furnitureStore\DAO;
use furnitureStore\DAO\Base;
use furnitureStore\Utils\Utils;

class TagDAO extends BaseDAO {
  private $logger;

	public function __construct($log) {
     $this->logger = $log;
  }

  public function getLogger() {
  	return $this->logger;
	}

  public function listAllTags() {
    $sql = "SELECT * FROM tag";

    return $this -> selectDB( $sql, [], "furnitureStore\Model\Modelo", "GET ALL TAGS", false);
  }

  public function insertTag($name) {
    $sql = "INSERT INTO tag (name) VALUES (:name)";
    return $this -> insertDB( $sql, [ ':name' => $name ], "INSERT TAG", false);
  }

  public function getTagById($id) {
    $sql = "SELECT * FROM tag WHERE id_tag = :id";
    return $this -> selectDB( $sql, [ ':id' => $id ], "furnitureStore\Model\Modelo", "FIND TAG BY ID: " . $id, false );
  }

  public function updateTag( $tag ) {
      $sql = "UPDATE tag SET name = :name WHERE id_tag = :id";
      $id = $this->updateDB($sql, [ ':name' => $tag -> name, ':id' => $tag -> id_tag ], "UPDATE TAG ID: " . $tag -> id_tag, false );
      if($id != ""){
  			return true;
  		}
  		return false;
  }

  public function getTagByName( $name ) {
    $sql = "SELECT * FROM tag WHERE UPPER(name) = UPPER(:name) ";
    return $this -> selectDB( $sql, [ ':name' => $name], "furnitureStore\Model\Modelo", "FIND TAG BY NAME: " . $name, false );
  }

  public function getTagByPartialName( $name ) {
    $sql = "SELECT * FROM tag WHERE UPPER(name) LIKE UPPER(:name) ";
    return $this -> selectDB( $sql, [ ':name' => '%' . $name . '%'], "furnitureStore\Model\Modelo", "FIND TAG BY NAME: " . $name, false );
  }

  public function deleteTag( $id ) {
    $sql = "DELETE FROM tag WHERE id_tag = :id_tag";
    return $this->deleteDB($sql, array(':id_tag' => $id), "DELETE TAG ID: $id ", false);
  }
}

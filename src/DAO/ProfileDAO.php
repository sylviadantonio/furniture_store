<?php
namespace furnitureStore\DAO;
use furnitureStore\DAO\Base;

class ProfileDAO extends BaseDAO {
	private $logger;

	public function __construct($log) {
     $this->logger = $log;
  }

  public function getLogger() {
  	return $this->logger;
	}

	public function insertProfile($name) {
		$sql = "INSERT INTO " .
				"profile (name) " .
				" VALUES (:name)";
		return $this->insertDB($sql, array(':name' => $name), "INSERT PROFILE", false);
	}

	public function recoverProfileByName($name) {
		$sql = "SELECT * " .
				"FROM profile p " .
				"WHERE
				p.name = :name";
		return $this->selectDB($sql, array(':name' => $name),
			"furnitureStore\Model\Modelo", "RECOVER PROFILE BY NAME: " . $name, false);
	}

	public function updateProfile($profile){
		$sql = "UPDATE " .
				"profile SET " .
					"name = :name " .
				"WHERE id_profile = :id_profile";
		$id = $this->updateDB($sql, array(':id_profile' => $profile->id_profile, ':name' => $profile->name
	), "UPDATE PROFILE ID: " . $profile->id_profile, false);
		if($id != ""){
			return true;
		}
		return false;
	}

	public function deleteProfile($id) {
		$sql = "DELETE " .
				"FROM profile " .
				"WHERE id_profile = :id_profile ";
		return $this->deleteDB($sql, array(':id_profile' => $id), "DELETE PROFILE ID: $id ");
	}

	public function retrieveProfilePorId($id) {
		$sql = "SELECT * " .
				"FROM profile p " .
				"WHERE
				p.id_profile = :id";
		return $this->selectDB($sql, array(':id' => $id),
			"furnitureStore\Model\Modelo", "RECOVER PROFILE BY ID: " . $id, false);
	}

	public function retrieveAllProfiles() {
		$sql = "SELECT * " .
				"FROM profile ORDER BY name";
		return $this->selectDB($sql, null, "furnitureStore\Model\Modelo", "RECOVER ALL PROFILES");
	}

	public function checkUserByProfile($idProfile) {
		$sql = "SELECT id FROM user WHERE id_profile = :id_profile";
		$result = $this->selectDB($sql, array(':id_profile' => $idProfile), "furnitureStore\Model\Modelo", "CHECK IF EXISTS USER WITH PROFILE ID:" . $idProfile);
		return !empty($result);
	}
}
?>

<?php
namespace furnitureStore\DAO;
use furnitureStore\DAO\Base;
use furnitureStore\Utils\Utils;

class UserDAO extends BaseDAO {
  private $logger;

	public function __construct($log) {
     $this->logger = $log;
  }

  public function getLogger() {
  	return $this->logger;
	}


  public function insertUser($userObj) {
    //var_dump($userObj); die;
      $sql = "INSERT INTO user " .
          "(username, " .
            "email, " .
            "password, " .
            "id_profile, " .
            "status, " .
            "date_recorded ) " .
          "VALUES ( ".
            ":username, " .
            ":email, " .
            ":password, " .
            ":id_profile, " .
            ":status, " .
            ":date_recorded )";
            if(!isset($userObj->status)){
              $userObj->status = 1;
            }

      return $this->insertDB($sql, array(
              ':username' => $userObj->username,
              ':email' => $userObj->email,
              ':password' => $userObj->password,
              ':id_profile' => $userObj->id_profile,
              ':status' => $userObj->status,
              ':date_recorded' => $userObj->date_recorded), "INSERT USER", false);
  }

  public function retrieveUserByEmail($email) {
    $sql = "SELECT * " .
      "FROM user u " .
      "WHERE u.email = :email AND u.status = 1";
    return $this->selectDB($sql, array(":email" => $email),
      "furnitureStore\Model\Modelo", "RECOVER USER BY EMAIL: " . $email, false);
  }

  public function updateUser($userObj) {
      $sql = "UPDATE user SET " .
            "username = :username, " .
            "email = :email, " .
            "password = :password, " .
            "id_profile = :id_profile " .
            "WHERE id_user = :id_user";
      $id = $this->updateDB($sql, array(
              ':id_user' => $userObj->id_user,
              ':username' => $userObj->username,
              ':email' => $userObj->email,
              ':password' => $userObj->password,
              ':id_profile' => $userObj->id_profile),
              "UPDATE USER ID: " . $userObj->id_user, false);

      if($id != "") {
        return true;
      }
      return false;
  }

  public function deleteUser($id) {
      $sql = "DELETE " .
          "FROM user " .
          "WHERE id_user = :id_user ";
      return $this->deleteDB($sql, array(':id_user' => $id), "DELETE USER BY ID: $id_user ", false);
  }

  public function retrieveUserById($id) {
  		$sql = "SELECT u.* " .
  				"FROM user u " .
  				"WHERE
  				u.id_user = :id_user AND u.status = 1";
  		return $this->selectDB($sql, array(':id_user' => $id),
  			"furnitureStore\Model\Modelo", "RECOVER USER BY ID: " . $id, false);
  }

  public function recoverQuantityOfActiveUsers() {
      $sql = "SELECT COUNT(*) AS total " .
          "FROM user u " .
          "WHERE
          u.status = 1 ";
      return $this->selectDB($sql, null,
        "furnitureStore\Model\Modelo", "RECOVER TOTAL OF ACTIVE USERS", false);
  }

  public function retrieveUserAndProfileById($id) {
  		$sql = "SELECT u.* " .
  				"FROM user u " .
          "JOIN profile p ON " .
          "u.id_profile = p.id " .
  				"WHERE u.id_user = :id_user AND u.status = 1";
  		return $this->selectDB($sql, array(':id_user' => $id),
  			"furnitureStore\Model\Modelo", "RECOVER USER WITH PROFILE BY USER ID: " . $id, false);
  }

  public function retrieveAllUsersWithProfile() {
		$sql = "SELECT u.*, p.name AS nameprofile " .
				" FROM user u JOIN profile p ON " .
        " u.id_profile = p.id_profile WHERE u.status = 1 ORDER BY u.username";
    $users = $this->selectDB($sql, null, "furnitureStore\Model\Modelo", "RECOVER ALL USERS WITH PROFILE");
    $sql2 = "SELECT * FROM client c WHERE c.id_user = :id_user";
    $finalList = array();
    foreach ($users as $u) {
      $u -> password ="";
      if($u -> id_profile == 3) {
        $client = $this->selectDB( $sql2, [':id_user' => $u -> id_user], "furnitureStore\Model\Modelo", "RECOVER CLIENT INFO FROM USER: " . $u -> id_user, false);
        if( $client != null ) {
          $u->client = $client[0];
        }
      }
      array_push($finalList, $u);
    }
    return $finalList;
	}

  public function recoverLogin($user) {
    $sql ="SELECT u.*, p.name AS nameprofile " .
				  " FROM user u JOIN profile p ON " .
          " u.id_profile = p.id_profile " .
          "WHERE u.email = :email AND u.status = 1";
    $userRec = $this->selectDB($sql, array(":email" => $user->login_email),
      "furnitureStore\Model\Modelo", "RECOVER USER BY EMAIL " , false);

      if(count($userRec) > 0) {
        if(password_verify($user->login_senha, $userRec[0]->password)) {
          $userRec[0]->password = "";
          return $userRec[0];
        }
      } else {
        return false;
      }
  }

  public function changeUserStatus ($id_user, $status) {
    $sql = "UPDATE user SET status = :status WHERE id_user = :id_user";
    $rows = $this->updateDB($sql, array(":status" => $status, ":id_user" => $id_user),
    "CHANGE USER STATUS ID: " . $id_user . ", STATUS: " . $status, false);
    return $rows > 0;
  }

  public function verifypassword($user) {
    $sql = "SELECT u.password FROM user u WHERE u.id_user = :id_user";
    $passwordRec = $this->selectDB($sql, array(":id_user" => $user->id_user), null, "CHECK IF PASSWORD IS VALID", false);
    return password_verify($user->passwordAtual, $passwordRec[0]->password);
  }

  public function changeUserPassword($user) {
      $sql = "UPDATE user SET password = :password WHERE id_user = :id_user";
      $rows = $this->updateDB($sql, array(':password'=>$user->password, ':id_user' =>$user->id_user),
            "CHANGE USER PASSWORD BY USER ID: " . $user->id_user, false);
      return $rows > 0;
  }

}

?>

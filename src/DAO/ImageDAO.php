<?php
namespace furnitureStore\DAO;
use furnitureStore\Utils\Globals;

class ImageDAO extends BaseDAO {
  private $logger;

  public function __construct($log){
     $this->logger = $log;
  }

  public function getLogger() {
    return $this->logger;
  }

  public function removeArquivosServer($arquivosArray) {
    $caminhocompleto = $this->defineDiretorio();
    foreach ($arquivosArray as $imgRm) {
      if(file_exists($caminhocompleto ."/". $imgRm)){
        if(!unlink($caminhocompleto ."/". $imgRm)) {
          $this->logger->error("Imagem não excluida: " . $caminhocompleto ."/". $imgRm);
          return false;
        }
      }
    }
    return true;
  }

  public function adicionaArquivosServer($arquivosArray){
    $diretorio = $this->defineDiretorio();
    if(!is_dir($diretorio)) {
      $this->logger->error("Diretorio nao encontrado: " . $diretorio);
    } else if(!is_writable($diretorio)) {
        $this->logger->error("Nao e possivel gravar neste diretorio: " . $diretorio);
    } else {
      for($i = 0; $i < count($arquivosArray); $i++) {
        if(!file_put_contents($diretorio ."/". $arquivosArray[$i][0], $arquivosArray[$i][1]))
        {
          $this->logger->error("Erro ao inserir imagem no servidor.");
          return false;
        }
      }
      return true;
    }
  }

  private function defineDiretorio() {
    if(isset($_ENV["TEST"]) && $_ENV["TEST"] == "true"){
      //Se for ambiente de testes, não faz nada
      $this->logger->info("upload de fotos em ambiente de testes");
      return "";
    } else {
      if(isset($_SERVER["PRD"]) && $_SERVER["PRD"] == "true"){
        $this->logger->info("upload de fotos em ambiente de produção");
        return Globals::$IMG_UPLOAD_SERVER_PATH_PRD . Globals::$PASTA_IMG_UPLOAD;
      } else {
        $this->logger->info("upload de fotos em ambiente de desenvolvimento");
        return Globals::$IMG_UPLOAD_SERVER_PATH . Globals::$PASTA_IMG_UPLOAD;
      }
    }
  }
}
?>

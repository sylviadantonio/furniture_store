<?php
namespace furnitureStore\DAO;
use \PDO;

abstract class BaseDAO {
    private $logger;

    private function __clone(){}
    public function __destruct() {
        $this->disconnect();
        foreach ($this as $key => $value) {
            unset($this->$key);
        }
    }
    abstract public function getLogger();

    private static $dbtype   = "mysql";
    private static $host     = "localhost";
    private static $port     = "3306";
    private static $user     = "root";
    private static $password = "Sylvi@123";
    private static $db       = "furniture_store";

    private static $hostPRD     = "***HOST***";
    private static $portPRD     = "3306";
    private static $userPRD     = "***USUARIO***";
    private static $passwordPRD = "***SENHA***";
    private static $dbPRD       = "furniture_store";

    private static $dbtypeTST   = "mysql";
    private static $hostTST     = "localhost";
    private static $portTST     = "3306";
    private static $userTST     = "root";
    private static $passwordTST = "Sylvi@123";
    private static $dbTST       = "furniture_store_tst";

    private function getDBType()  {return self::$dbtype;}
    private function getHost()    {return self::$host;}
    private function getPort()    {return self::$port;}
    private function getUser()    {return self::$user;}
    private function getPassword(){return self::$password;}
    private function getDB()      {return self::$db;}

    private function getHostPRD()    {return self::$hostPRD;}
    private function getPortPRD()    {return self::$portPRD;}
    private function getUserPRD()    {return self::$userPRD;}
    private function getPasswordPRD(){return self::$passwordPRD;}
    private function getDBPRD()      {return self::$dbPRD;}

    private function getDBTypeTST()  {return self::$dbtypeTST;}
    private function getHostTST()    {return self::$hostTST;}
    private function getPortTST()    {return self::$portTST;}
    private function getUserTST()    {return self::$userTST;}
    private function getPasswordTST(){return self::$passwordTST;}
    private function getDBTST()      {return self::$dbTST;}

    private function connect(){
      if(isset($_ENV["TEST"]) && $_ENV["TEST"] == "true"){
          //@$this->getLogger()->info("AMBIENTE DE TESTES");
        	try {
            $this->conexao = new PDO($this->getDBTypeTST().":host=".$this->getHostTST().";port=".$this->getPortTST().";dbname=".$this->getDBTST().";charset=utf8", $this->getUserTST(), $this->getPasswordTST(),
                    array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
            $this->conexao->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->conexao->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
          } catch (PDOException $i) {
              $this->conexao = "";
              die("Erro: <code>" . $i->getMessage() . "</code>");
          }
      } else {
        if(isset($_SERVER["PRD"]) && $_SERVER["PRD"] == "true"){
      		try {
            $this->conexao = new PDO($this->getDBType().":host=".$this->getHostPRD().";port=".$this->getPort().";dbname=".$this->getDBPRD().";charset=utf8", $this->getUserPRD(), $this->getPasswordPRD(),
                    array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
            $this->conexao->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->conexao->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
          } catch (PDOException $i) {
              $this->conexao = "";
              die("Erro: <code>" . $i->getMessage() . "</code>");
          }
      	} else {
    			try {
              $this->conexao = new PDO($this->getDBType().":host=".$this->getHost().";port=".$this->getPort().";dbname=".$this->getDB().";charset=utf8", $this->getUser(), $this->getPassword(),
                      array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
              $this->conexao->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
              $this->conexao->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    	        } catch (PDOException $i) {
                  $this->conexao = "";
    	            die("Erro: <code>" . $i->getMessage() . "</code>");
           		}
        	}
      }
      return ($this->conexao);
    }

    private function disconnect(){
        $this->conexao = null;
    }

    public function getConnection () {
      return $this->connect();
    }

    public function recuperaUltimoId () {
        $conexao = $this->connect();
        $rs = $conexao->lastInsertId();
        self::__destruct();
        return $rs;
    }

    public function selectDB($sql,$params=null,$class=null, $acao, $debug = false){
        try {
            //$query=$this->connect()->prepare($sql);
            $conexao = $this->connect();
            //print_r($conexao);die;
            if($conexao == null || $conexao == ""){
              print_r($conexao);die;
            }
            $query = $conexao->prepare($sql);
            $query->execute($params);
            if($debug){
                $this->debug($params, $query);
                die;
            }
            if(isset($class)){
                $rs = $query->fetchAll(PDO::FETCH_CLASS,$class);
            }else{
                $rs = $query->fetchAll(PDO::FETCH_OBJ);
            }
        } catch (\PDOException $Exception) {
          	$this->gravaELogaErro($acao, $Exception, $sql);
            self::__destruct();
            return null;
        }
        self::__destruct();
        return $rs;
    }

    public function selectCountDB($sql,$params=null,$acao, $debug=false){
        try {
            $query=$this->connect()->prepare($sql);
            $query->execute($params);
            if($debug){
                $this->debug($params, $query);
                die;
            }
      			$rs = $query->fetchAll(PDO::FETCH_OBJ);
      			return $rs[0]->TOTAL;
        } catch (\PDOException $Exception) {
            $this->gravaELogaErro($acao, $Exception, $sql);
            self::__destruct();
            return null;
        }
        self::__destruct();
        return $rs;
    }

    public function insertDB($sql, $params=null, $acao, $debug = false){
        try {
            $conexao=$this->connect();
            $query = $conexao->prepare($sql);
            if($debug){
                $this->debug($params, $query);
                die;
            }
            $query->execute($params);
            $rs = $conexao->lastInsertId();
        } catch (\PDOException $Exception) {
          $this->gravaELogaErro($acao, $Exception, $sql);
          self::__destruct();
          return null;
        }
        self::__destruct();
        return $rs;
    }

    private function gravaELogaErro($acao, $exception, $sql){
      @$this->getLogger()->info($sql." - " . $acao. " - " . $exception->getMessage() . " - " . $exception->getCode());
      $dao = new ErrorDAO($this->getLogger());
      $erro = new \stdClass();
      $erro->action = $acao;
      $erro->message = $exception->getMessage() . " - " . $exception->getCode();
      $erro->sqlerror = $sql;
      $dao->gravaErro($erro);
    }

    private function debug($params, $query){
      print_r($params);
      echo "<Br>";
      echo "<Br>";
      echo '<pre>'.htmlspecialchars($this->pdo_debugStrParams($query)).'</pre>';
      echo "<Br>";
      echo "<Br>";
      $query->debugDumpParams();
    }

    private function pdo_debugStrParams($stmt) {
	  ob_start();
	  $stmt->debugDumpParams();
	  $r = ob_get_contents();
	  ob_end_clean();
	  return $r;
	}

    public function updateDB($sql,$params=null, $acao, $debug = false){
        try {
        	$conexao=$this->connect();
          $query=$conexao->prepare($sql);
        	$return = $query->execute($params);
          if($debug){
              $this->debug($params, $query);
              die;
          }
          $rowNumber = $query->rowCount();
        } catch (\PDOException $Exception) {
            $this->gravaELogaErro($acao, $Exception, $sql);
            self::__destruct();
            return 0;
        }
        self::__destruct();
        if($rowNumber == 0){
          return $return;
        } else {
          return $rowNumber;
        }
    }

    /*Método delete que excluí valores do banco de dados retorna o número de linhas afetadas*/
    public function deleteDB($sql,$params=null, $acao, $debug = false){
        $query=$this->connect()->prepare($sql);
        try {
        	$return = $query->execute($params);
          if($debug){
              $this->debug($params, $query);
              die;
          }
        	$rowNumber = $query->rowCount();
        } catch (\PDOException $Exception) {
            $this->gravaELogaErro($acao, $Exception, $sql);
            self::__destruct();
            return 0;
        }
        self::__destruct();
        if($rowNumber == 0){
          return $return;
        } else {
          return $rowNumber;
        }
    }

    public function isTest(){
        if(isset($_ENV["TEST"]) && $_ENV["TEST"] == "true"){
          return true;
        }
        return false;
    }

    public function getHoje () {
        $today = getdate();
        return $today["year"]."-".$today["mon"]."-".$today["mday"]." ".$today["hours"].":".$today["minutes"].":".$today["seconds"];
    }
}
?>

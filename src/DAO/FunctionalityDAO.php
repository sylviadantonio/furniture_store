<?php
namespace furnitureStore\DAO;
use furnitureStore\DAO\Base;

class FunctionalityDAO extends BaseDAO {
	private $logger;

	public function __construct($log) {
     $this->logger = $log;
  }

  public function getLogger() {
  	return $this->logger;
	}

  public function insertFunctionality($url, $name, $show_menu, $id_menu_category) {

		$sql = "INSERT INTO " .
				"functionality (url, name, show_menu, id_menu_category) " .
				" VALUES (:url, :name, :show_menu, :id_menu_category)";
		return $this->insertDB($sql, array(':url' => $url, ':name' => $name,
		':show_menu' => $show_menu, ':id_menu_category' => $id_menu_category), "INSERT FUNCTIONALITY", false);
	}

	public function insertAccessPermissions($id_functionality, $id_profile) {
		$sql = "INSERT INTO " .
				"access_permission (id_functionality, id_profile) " .
				" VALUES (:func, :profile)";
		return $this->insertDB($sql, array(':func' => $id_functionality, ':profile' => $id_profile), "INSERT ACCESS PERMISSIONS", false);
	}

	public function retrievePermissionByFunctionailyId($id) {
		$sql = "SELECT * " .
				"FROM access_permission p " .
				"WHERE
				p.id_functionality = :id";
		return $this->selectDB($sql, array(':id' => $id),
			"furnitureStore\Model\Modelo", "RECOVER ACCESS PERMISSION BY ID FUNCTIONALITY: " . $id, false);
	}

	public function retrievePermissionsByProfileId($id) {

		$sql = "SELECT f.*, c.name AS menu_category, c.position
			FROM access_permission p, functionality f
				left outer join menu_category c on c.id_menu_category = f.id_menu_category
				WHERE f.id_functionality = p.id_functionality AND p.id_profile = :id
					ORDER BY c.position, f.name";

		return  $this->selectDB($sql, array(':id' => $id),
			"furnitureStore\Model\Modelo", "RECOVER ACCESS PERMISSION BY ID_PROFILE: " . $id, false);
	}

  public function deleteFunctionality($id) {
		$sql = "DELETE " .
				"FROM functionality " .
				"WHERE id_functionality = :id ";

		return $this->deleteDB($sql, array(':id' => $id), "DELETE FUNCIONALIDADE POR: $id ", false);
	}

	public function deleteAccessPermissionByIdFunctionality($id_functionality) {
		$sql = "DELETE " .
				"FROM access_permission " .
				"WHERE id_functionality = :id_functionality ";
		return $this->deleteDB($sql, array(':id_functionality' => $id_functionality), "DELETE ACCESS PERMISIONS BY ID FUNCTIONALITY: $id_functionality ", false);
	}

  public function retrieveAllFunctionalities() {
		$sql = "SELECT f.*, c.name as categoria FROM functionality f left outer join menu_category c on c.id_menu_category = f.id_menu_category ORDER BY categoria";
		$result = $this->selectDB($sql, null, "furnitureStore\Model\Modelo", "RETRIEVE ALL FUNCTIONALITIES", false);
		//var_dump( $result ); die;
		return $result;
	}

	public function retrieveAllMenuCategories() {
		$sql = "SELECT * " .
				"FROM menu_category ORDER BY name";
		return $this->selectDB($sql, null, "furnitureStore\Model\Modelo", "RETRIEVE ALL MENU CATEGORIES");
	}

	public function retrieveFunctionalityByUrl($url) {
		$sql = "SELECT * " .
				"FROM functionality p " .
				"WHERE
				p.url = :url";
		return $this->selectDB($sql, array(':url' => $url),
			"furnitureStore\Model\Modelo", "RETRIEVE FUNCTIONALITY BY URL: " . $url, false);
	}

	public function updateFunctionality($functionality){
		$sql = "UPDATE " .
				"functionality SET " .
					"url = :url,
					name = :name,
					show_menu = :show_menu,
					id_menu_category = :id_menu_category " .
				"WHERE id_functionality = :id";

		$id = $this->updateDB($sql,
		array(':id' => $functionality->id_functionality, ':url' => $functionality->url,
		':name' => $functionality->name, ':show_menu' => $functionality->show_menu, ':id_menu_category' => $functionality->id_menu_category
	), "ATUALIZA FUNCIONALIDADE ID: " . $functionality->id_functionality, false);
		if($id != ""){
			return true;
		}
		return false;
	}

	public function retrieveFunctionalityById($id) {
		$sql = "SELECT * FROM functionality p WHERE p.id_functionality = :id";
		return $this->selectDB($sql, array(':id' => $id),
			"furnitureStore\Model\Modelo", "RETRIEVE FUNCTIONALITY BY ID: " . $id, false);
	}

}

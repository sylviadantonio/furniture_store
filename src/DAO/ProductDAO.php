<?php
namespace furnitureStore\DAO;
use furnitureStore\DAO\Base;
use furnitureStore\Utils\Utils;

class ProductDAO extends BaseDAO {
  private $logger;

	public function __construct($log) {
     $this->logger = $log;
  }

  public function getLogger() {
  	return $this->logger;
	}


  /**
  * If the friendly url exist return true,
  * if doesn't exist return false
  **/
  public function checkFriendlyURL($url) {
    $sql = "SELECT * FROM product WHERE friendly_url = :url";
    $result = $this -> selectDB($sql, array(':url' => $url),
      "furnitureStore\Model\Modelo", "CHECK IF EXIST URL: " . $url, false);

      return !empty($result);
  }

  public function insertProductPicture( $idProduct, $imgName, $folder, $isMain, $title ){
    $sql = "INSERT INTO product_picture ( id_product, img, folder, cover, title)
              VALUES (:id_product, :img, :folder, :cover, :title)";

      return $this->insertDB($sql,
          [':id_product' => $idProduct,
          ':img' => $imgName,
          ':folder' => $folder,
          ':cover' => $isMain,
          ':title' => $title
        ], "INSERT NEW PRODUCT PICTURE", false);

  }
}

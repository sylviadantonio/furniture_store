<?php
namespace furnitureStore\DAO;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;


class MailDAO {
  private $logger;
  private $mail;

  public function __construct($log) {
     $this->logger = $log;
     $this->mail = new PHPMailer();
     # Define os dados do servidor e tipo de conexão
     $this->mail->IsSMTP(); // Define que a mensagem será SMTP
     $this->mail->Host = "br922.hostgator.com.br"; # Endereço do servidor SMTP
     $this->mail->Port = 587; // Porta TCP para a conexão
     $this->mail->SMTPAutoTLS = false; // Utiliza TLS Automaticamente se disponível
     $this->mail->SMTPAuth = true; # Usar autenticação SMTP - Sim
     $this->mail->SMTPDebug = 2;
     $this->mail->Username = '***EMAIL***'; # Usuário de e-mail
     $this->mail->Password = '***SENHA***'; // # Senha do usuário de e-mail
     $this->mail->CharSet = 'UTF-8';
     $this->mail->Encoding = 'base64';

     # Define o remetente (você)
     $this->mail->From = "***EMAIL***";
     $this->mail->FromName = "***NAME***"; // Seu nome
  }

  public function getLogger() {
    return $this->logger;
  }

  public function enviaEmail($emailInfo) {
    # Define os destinatário(s)
    $this->mail->AddAddress($emailInfo->destinatario->email, $emailInfo->destinatario->nome); # Os campos podem ser substituidos por variáveis
    #$mail->AddAddress('webmaster@nomedoseudominio.com'); # Caso queira receber uma copia
    #$mail->AddCC('ciclano@site.net', 'Ciclano'); # Copia
    #$mail->AddBCC('fulano@dominio.com.br', 'Fulano da Silva'); # Cópia Oculta

    # Define os dados técnicos da Mensagem
    $this->mail->IsHTML(true); # Define que o e-mail será enviado como HTML
    #$mail->CharSet = 'iso-8859-1'; # Charset da mensagem (opcional)

    # Define a mensagem (Texto e Assunto)
    $this->mail->Subject = $emailInfo->titulo; # Assunto da mensagem
    $this->mail->Body = $emailInfo->corpo;

    # Define os anexos (opcional)
    #$mail->AddAttachment("c:/temp/documento.pdf", "documento.pdf"); # Insere um anexo

    # Envia o e-mail
    try{
      return $this->mail->Send();

    } catch(Exception $ex) {
      $this->logger->error("Erro ao enviar email: " . $ex);
      $this->logger->error("Mais info: " . $this->mail->ErrorInfo);
      return false;
    } finally {
      # Limpa os destinatários e os anexos
      $this->mail->ClearAllRecipients();
      $this->mail->ClearAttachments();
    }
  }
}
?>

<?php
use Slim\App;
use furnitureStore\Controller\AppAuth;

return function (App $app) {
  $container = $app->getContainer();
  $app->add(new AppAuth($container));
};

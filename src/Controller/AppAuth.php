<?php
namespace furnitureStore\Controller;
use furnitureStore\DAO;

class AppAuth {
	private $container;

	public function __construct($app) {
		$this->container = $app;
		$this->publicUrls = array();
        $this->whiteList =
        array('\/login', '\/logout');
    }

	public function __invoke($request, $response, $next){
    	$uri = $request->getUri();
			$path = explode("/", $uri);
			if(isset($_ENV["SKIP"]) && $_ENV["SKIP"] == "true"){
				return $next($request, $response);
			}
			if($path[3] == "" || $this->isPublicUrl($uri)){
				return $next($request, $response);
			} else {
    		if(isset($_SESSION['usuario'])){
						$lista = $_SESSION['usuario']->urlsPermitidas;
						if($this->arrayContains($lista, $uri)){
							return $next($request, $response);
						} else {
							$msg = 'Você não possui autorização para acessar essa área.';
							$this->container->flash->addMessage('error', $msg);
		    			return $response->withStatus(200)->withHeader('Location', '/admin');
						}
    		} else {
    			$msg = 'É necessário fazer o login pra acessar essa área.';
					$this->container->flash->addMessage('error', $msg);
    			return $response->withStatus(200)->withHeader('Location', '/login');
    		}
			}
    }

		private function arrayContains($lista, $str){
			foreach ($lista as $url) {
			    if (strpos($str, $url->url) !== FALSE) {
			        return true;
			    }
			}
			return false;
		}

    private function isPublicUrl($url) {
    		$patterns_flattened = implode('|', $this->whiteList);
        $matches = null;
        preg_match('/' . $patterns_flattened . '/', $url, $matches);
        return (count($matches) > 0);
    }

}
?>

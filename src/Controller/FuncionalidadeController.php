<?php
namespace furnitureStore\Controller;
use furnitureStore\DAO;
use furnitureStore\Utils\Globals;

class FuncionalidadeController extends BaseController {
  private $dao;
  private $daoProfile;

  public function __construct($app) {
    parent::__construct($app);
    $this->dao = new DAO\FunctionalityDAO($this->logger);
    $this->daoProfile = new DAO\ProfileDAO($this->logger);
  }

  public function inicio($request, $response, $args){
      $funcionalidade = "";
      if(isset($_SESSION[Globals::$FUNCIONALIDADE])){
        $funcionalidade = $_SESSION[Globals::$FUNCIONALIDADE];
        $_SESSION[Globals::$FUNCIONALIDADE] = "";
      }

      $listaPerfis = $this->daoProfile->retrieveAllProfiles();
      $listaFuncionalidades = $this->dao->retrieveAllFunctionalities();
      $listaCategorias = $this->dao->retrieveAllMenuCategories();
      //var_dump($listaFuncionalidades); die;
      return $this->container->view->render($response, Globals::$FUNCIONALIDADE_TEMPLATE, [Globals::$FUNCIONALIDADES => $listaFuncionalidades,
          Globals::$PERFIS => $listaPerfis, Globals::$FUNCIONALIDADE => $funcionalidade,
          'categorias'=>$listaCategorias]);
  }

  public function edita($request, $response, $args){
      @$funcionalidade->id_functionality = $args['id'];
      $funcionalidade = $this->dao->retrieveFunctionalityById($funcionalidade->id_functionality);
      $listaPerfisAutorizados = "";
      $listaPerfis = $this->daoProfile->retrieveAllProfiles();
      $listaProfileFim = array();
      if($funcionalidade == "" || count($funcionalidade) <= 0){
        $this->mensagemErroWeb("Functionality was not found.");
      } else {
        $funcionalidade = $funcionalidade[0];
        $listaPerfisAutorizados = $this->dao->retrievePermissionByFunctionailyId($funcionalidade->id_functionality);

        foreach($listaPerfis AS $perfil){
          foreach($listaPerfisAutorizados AS $perfilAut){
            if($perfil->id_profile == $perfilAut->id_profile){
              $perfil->marcado = true;
              array_push($listaProfileFim, $perfil);
            }
          }
        }
      }
      $listaFuncionalidades = $this->dao->retrieveAllFunctionalities();
      $listaCategorias = $this->dao->retrieveAllMenuCategories();
      return $this->container->view->render($response, Globals::$FUNCIONALIDADE_TEMPLATE, [
        Globals::$FUNCIONALIDADES => $listaFuncionalidades, Globals::$FUNCIONALIDADE => $funcionalidade,
      Globals::$PERFIS => $listaPerfis, 'categorias'=>$listaCategorias]);
  }

  public function apaga($request, $response, $args){
       $id = $args['id'];
      //TODO antes de apagar, verificar se tem algum usuário ou regra nesse perfil.
      if($this->dao->deleteAccessPermissionByIdFunctionality($id)) {
        if($this->dao->deleteFunctionality($id)){
          $this->mensagemSucessoWeb("Functionality successfully deleted.");
        } else {
          $this->mensagemErroWeb("Oops, there was an error deleting this functionality.");
        }
      } else {
        $this->mensagemErroWeb("Oops, there was an error deleting the access permissions for this functionality.");
      }
      return $response->withStatus(200)->withHeader(Globals::$LOCATION, Globals::$FUNCIONALIDADE_URL);
  }

  private function validaFuncionalidade($funcionalidade){
    $success = true;
    if(!isset($funcionalidade->url) || $funcionalidade->url == ""){
      $this->mensagemErroWeb("It is necessary to assign an URL to the functionality.");
      $success = false;
    }

    if(!isset($funcionalidade->perfilautorizado) || empty($funcionalidade->perfilautorizado)){
      $this->mensagemErroWeb("It is necessary to assign at least one profile to the functionality.");
      $success = false;
    }
    if(!isset($funcionalidade->name) || $funcionalidade->name == ""){
      $this->mensagemErroWeb("It is necessary to assign a name to the functionality");
      $success = false;
    }

    @$funcionalidadeTemp = $this->dao->retrieveFunctionalityByUrl($funcionalidade->url);
    if(!empty($funcionalidadeTemp)){
      $funcionalidadeTemp = $funcionalidadeTemp[0];
    }

    if($funcionalidade->id_functionality == ""){//Novo perfil
      if(!empty($funcionalidadeTemp)){
        $this->mensagemErroWeb("One functionality already exists with this URL.");
        $success = false;
      }
    } else {
      if(!empty($funcionalidadeTemp) && ($funcionalidade->id_functionality != $funcionalidadeTemp->id_functionality)){
        $this->mensagemErroWeb("One functionality already exists with this URL.");
        $success = false;
      }
    }
    return $success;
  }

  private function gravaPerfisAutorizados($funcionalidade){
    $total = count($funcionalidade->perfilautorizado);
    $sucesso = 0;
    foreach($funcionalidade->perfilautorizado AS $perfil){
      if($this->dao->insertAccessPermissions($funcionalidade->id_functionality, $perfil)){
        $sucesso++;
      } else {
        $this->logger->info("Oops, there was an error on recording profile id " . $perfil);
      }
    }
    if($total != $sucesso){
      $this->mensagemSucessoWeb("Functionality inserted successfully, however there was a problem on inserting the authorized profiles.");
    } else {
      //se tudo der certo atualizao usuario logado
      $this->daoFunc = new DAO\FunctionalityDAO($this->logger);
      $lista = $this->daoFunc->retrievePermissionsByProfileId($_SESSION['usuario']->id_profile);
      $_SESSION['usuario']->urlsPermitidas = $lista;
      $this->mensagemSucessoWeb("Functionality inserted successfully.");
    }
  }

  public function gravar($request, $response){
      $funcionalidade = $request->getParsedBody();
      $funcionalidade = json_decode(json_encode($funcionalidade), FALSE);

      if( $this -> validaFuncionalidade( $funcionalidade ) ) {
        if( !isset( $funcionalidade->show_menu ) ){
          $funcionalidade->show_menu = 0;
        } else {
          $funcionalidade->show_menu = 1;
        }
        if( !isset( $funcionalidade->id_menu_category )
            || $funcionalidade->id_menu_category == '' ) {
              $funcionalidade->id_menu_category = null;
        }
        if( $funcionalidade->id_functionality == "" ){//Novo perfil
          $id = $this->dao->insertFunctionality(
              $funcionalidade->url, $funcionalidade->name, $funcionalidade->show_menu, $funcionalidade->id_menu_category);
            if( $id != "" ){
              $funcionalidade->id_functionality = $id;
              $this->gravaPerfisAutorizados($funcionalidade);
            } else {
              $this->mensagemErroWeb("Oops, there was an error inserting this functionality.");
            }
        } else {//Edição
            $id = $this->dao->updateFunctionality($funcionalidade);
            if($id != ""){
              $this->dao->deleteAccessPermissionByIdFunctionality($funcionalidade->id_functionality);
              $this->gravaPerfisAutorizados($funcionalidade);
            } else {
              $this->mensagemErroWeb("Oops, there was an error updating this functionality.");
            }
        }
      } else {
        if(!isset($funcionalidade->id_functionality) || $funcionalidade->id_functionality == ""){
          $_SESSION[Globals::$FUNCIONALIDADE] = $funcionalidade;
          return $response->withStatus(200)->withHeader(Globals::$LOCATION, Globals::$FUNCIONALIDADE_URL);
      } else {
        return $response->withStatus(200)->withHeader(Globals::$LOCATION, Globals::$FUNCIONALIDADE_URL .'/' . $funcionalidade->idfuncionalidade);
      }
    }

      return $response->withStatus(200)->withHeader(Globals::$LOCATION, Globals::$FUNCIONALIDADE_URL);
  }
}
?>

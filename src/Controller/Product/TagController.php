<?php
namespace furnitureStore\Controller\Product;
use furnitureStore\Controller\BaseController;
use furnitureStore\DAO;
use furnitureStore\Utils\Globals;
use furnitureStore\Utils\Utils;

class TagController extends BaseController {

  private $dao;
  private $tagList;

  public function __construct($app) {
    parent::__construct($app);
    $this->dao = new DAO\TagDAO($this->logger);
    $this->tagList = $this -> dao -> listAllTags();
  }

  public function start($request, $response, $args){

    return $this -> container -> view -> render(
      $response, Globals::$TAG_TEMPLATE,
      [ Globals::$TAGS => $this->tagList ] );
  }

  public function insert( $request, $response ) {
    $tag = $request->getParsedBody();
    $tag = json_decode(json_encode($tag), FALSE);

    if( !isset( $tag -> name ) || $tag -> name == "" ) {
        $this -> mensagemErroWeb( "Tag name is mandatory." );
    } else {
      //check if there's already a tag with this name
      $tagTemp = $this -> dao -> getTagByName( $tag -> name );

      if( $tagTemp != null || !empty( $tagTemp ) ) {
        $this -> mensagemErroWeb( "This tag name is already in use." );
      } else {
        $tag -> name = ucfirst( $tag -> name );

        if( !isset( $tag -> id_tag ) || $tag -> id_tag == "" ) {
          $id_tag = $this -> dao -> insertTag( $tag -> name );
          if( $id_tag == null || $id_tag == "" ) {
            $this -> mensagemErroWeb( "Oops, there was an error inserting a new tag." );
          } else {
            $this -> mensagemSucessoWeb( "Tag inserted successfully." );
          }
        } else {
          if( $this -> dao -> updateTag( $tag ) ) {
            $this -> mensagemSucessoWeb( "Tag edited successfully." );
          } else {
            $this -> mensagemErroWeb( "Oops, there was an error editing this tag." );
          }
        }
      }
    }
    return $response -> withStatus(200) -> withHeader( Globals::$LOCATION, Globals::$TAG_URL );
  }

  public function insertAsynchronous( $request, $response ) {
    $tag = $request->getParsedBody();
    $tag = json_decode(json_encode($tag), FALSE);

    if( isset( $tag -> name ) && $tag -> name != "" ) {
      //check if there's already a tag with this name
      $tagTemp = $this -> dao -> getTagByName( $tag -> name );
      if( $tagTemp == null || empty( $tagTemp ) ) {
        $tag -> name = ucfirst( $tag -> name );
        $id_tag = $this -> dao -> insertTag( $tag -> name );
        if( $id_tag == null || $id_tag == "" ) {
          return $response -> withJson( ["error"], 201 );
        } else {
          return $response -> withJson( [ "success" => [ 'id' => $id_tag, 'name' => $tag -> name ] ], 201 );
        }
      }
    }
  }

  public function edit( $request, $response, $args ) {
    $id_tag = $args[ 'id' ];

    $tag = $this -> dao -> getTagById( $id_tag );

    if( $tag == null || empty( $tag ) ) {
      $this -> mensagemErroWeb( "Tag not found." );
      return $response -> withStatus(200) -> withHeader( Globals::$LOCATION, Globals::$TAG_URL );
    }

    return $this -> container -> view -> render (
        $response, Globals::$TAG_TEMPLATE,
        [ Globals::$TAGS => $this->tagList,
          Globals:: $TAG => $tag[0] ]);
  }

  public function delete ( $request, $response, $args ) {
    $id_tag = $args[ 'id' ];
    if( $this -> dao -> deleteTag( $id_tag ) ) {
      $this -> mensagemSucessoWeb( "Tag deleted successfully." );
    } else {
      $this -> mensagemErroWeb( "Oops, there was an error deleting this tag." );
    }

    return $response -> withStatus(200) -> withHeader( Globals::$LOCATION, Globals::$TAG_URL );
  }

  public function search ( $request, $response, $args ) {
      $text = $args[ 'text' ];
      $result = $this -> dao -> getTagByPartialName( $text );
      $returnItem = array();
      if( !empty( $result ) ) {
        foreach ( $result as $k => $it ) {
          $item['value'] = $it -> name;
          $item['data'] = $it -> id_tag;
          array_push($returnItem, $item);
        }
      } 
      return $response -> withJson( $returnItem, 201 );
  }


}

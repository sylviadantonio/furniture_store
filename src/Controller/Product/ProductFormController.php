<?php
namespace furnitureStore\Controller\Product;
use furnitureStore\Controller\BaseController;
use furnitureStore\Service;
use furnitureStore\Utils\Globals;
use furnitureStore\Utils\Utils;
use furnitureStore\Utils\ImageHelper;
use furnitureStore\Utils\QRCodeGenerator;
use chillerlan\QRCode\QROptions;
use chillerlan\QRCode\QRCode;

class ProductFormController extends BaseController {

  private $service;
  private $categoryService;

  public function __construct($app) {
    parent::__construct($app);
    $this -> service = new Service\ProductService( $this -> logger );
    $this -> categoryService = new Service\CategoryService ( $this -> logger );
  }

  public function start( $request, $response, $args ){
    $categoryList = $this -> categoryService -> findAllCategories( );
    $orderedCategories = $this -> categoryService -> orderCategory( $categoryList );
    return $this -> container -> view -> render( $response, Globals::$PRODUCT_FORM_TEMPLATE,
            [ Globals:: $CATEGORIES => $orderedCategories ] );
  }

  public function getQRCode( $request, $response, $args ) {
    $text = $args[ 'text' ];

    $options = new QROptions([
    	'version'      => 1,
    	'outputType'   => QRCode::OUTPUT_IMAGE_PNG,
    	'scale'        => 3,
      'imageBase64'  => false,
      'imageTransparent' => false,
      'imageTransparencyBG' => [255,255,255],
    ]);

    header('Content-type: image/png');
    $fileAddress = Globals::$FOLDER_QR . $text.'.png';
    $qrOutputInterface = new QRCodeGenerator(
        $options, ( new QRCode($options) ) -> getMatrix( $text ) );

    fopen($fileAddress,'0');
    // dump the output, with additional text
    $data = $qrOutputInterface -> dump( Globals:: $PUBLIC_FOLDER_PATH . $fileAddress, $text ) ;
    return json_encode( [ 'url' => $fileAddress ] );
  }

  public function checkFriendlyUrl( $request, $response, $args ) {
    return json_encode(
          [
            'url' => $this -> service -> prepareFriendyUrl( $args[ 'text' ], 0 )
          ] );
  }

  public function insert( $request, $response ) {
    $product = $request->getParsedBody();
    $product = json_decode(json_encode($product), FALSE);
    //var_dump($product);die;
    //insert images
    $helper = new ImageHelper( $this -> logger );
    //var_dump($product -> img_titles); die();
    if($helper -> treatImages( $_FILES, 1, $product -> main_img_title, $product -> img_titles ) ) {
      echo "everything went fine";
    }


    die;
  }
}

<?php
namespace furnitureStore\Controller\Product;
use furnitureStore\Controller\BaseController;
use furnitureStore\Utils\Globals;
use furnitureStore\Utils\Utils;
use furnitureStore\Service;

class CategoryController extends BaseController {

  private $categoryService;

  public function __construct($app) {
    parent::__construct($app);
    $this->categoryService = new Service\CategoryService($this->logger);
  }

  public function start($request, $response, $args){
    $categories = $this -> categoryService -> findAllCategories();
    $orderedCategory = $this -> orderCategory( $categories );

    return $this->container->view->render(
      $response, Globals::$CATEGORY_TEMPLATE, [
        Globals::$CATEGORIES => $orderedCategory,
        Globals::$LIST_CATEGORIES => $categories]);
  }

  public function insert( $request, $response ) {
    $category = $request->getParsedBody();
    $category = json_decode(json_encode($category), false);

    if( !isset( $category -> name ) || $category -> name == "" ) {
        $this -> mensagorderCategoryemErroWeb( "Category name is mandatory." );
    } else {
      //check if there's already a tag with this name
      $categoryTemp = $this -> categoryService -> getCategoryByName( $category -> name );
      $error = false;
      if( $categoryTemp != null || !empty( $categoryTemp ) ) {
        if( ( $categoryTemp -> id_cat_parent == $category -> id_cat_parent )
            && $category -> id_category !=  $categoryTemp -> id_category ) {
          $this -> mensagemErroWeb( "This category name is already in use." );
          $error = true;
        }
      }
      if( !$error ) {
        $category -> name = ucfirst( $category -> name );
        //echo('got here'); die;

        if( !isset( $category -> id_cat_parent ) || $category -> id_cat_parent == "" ) {
          $category -> id_cat_parent = null;
        }

        if( !isset( $category -> id_category ) || $category -> id_category == "" ) {
          $id_category = $this -> categoryService -> insertCategory( $category );
          if( $id_category == null || $id_category == "" ) {
            $this -> mensagemErroWeb( "Oops, there was an error inserting a new category." );
          } else {
            $this -> mensagemSucessoWeb( "Category inserted successfully." );
          }
        } else {
          if( $this -> categoryService -> updateCategory( $category ) ) {
            $this -> mensagemSucessoWeb( "Category edited successfully." );
          } else {
            $this -> mensagemErroWeb( "Oops, there was an error editing this category." );
          }
        }
      }$cat -> path ="root /";
    }
    return $response -> withStatus(200) -> withHeader( Globals::$LOCATION, Globals::$CATEGORY_URL );
  }


  public function edit( $request, $response, $args ) {
    $id_category = $args[ 'id' ];
    $category = $this -> categoryService -> getCategoryById( $id_category );

    if( $category == null || empty( $category ) ) {
      $this -> mensagemErroWeb( "Category not found." );
      return $response -> withStatus(200) -> withHeader( Globals::$LOCATION, Globals::$CATEGORY_URL );
    }
    $categories = $this -> categoryService -> findAllCategories();
    $orderedCategory = $this -> orderCategory( $categories, array() );

    return $this->container->view->render(
      $response, Globals::$CATEGORY_TEMPLATE,
                [ Globals::$CATEGORY => $category[0],
                  Globals::$CATEGORIES => $orderedCategory,
                  Globals::$LIST_CATEGORIES => $categories]);
  }

  public function orderCategory( $categoryList ) {
    return $this -> categoryService -> orderCategory( $categoryList);
  }

  public function delete ( $request, $response, $args ) {
    $id_category = $args[ 'id' ];
    if( $this -> categoryService -> deleteCategory( $id_category ) ) {
      $this -> mensagemSucessoWeb( "Category successfully deleted.");
    } else {
      $this -> mensagemSucessoWeb( "Oops there was an error on deleting this category.");
    }
    return $response -> withStatus(200) -> withHeader( Globals::$LOCATION, Globals::$CATEGORY_URL );
  }
}

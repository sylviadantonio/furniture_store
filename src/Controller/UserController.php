<?php
namespace furnitureStore\Controller;
use furnitureStore\DAO;
use furnitureStore\Utils\Utils;
use furnitureStore\Utils\Globals;

class UserController extends BaseController{

  private $dao;
  private $clientDAO;
  private $listPerfis;
  private $listaUsers;
  private $msgEmailJaExiste = "Already exist an user with this e-mail.";

  public function __construct($app) {
    parent::__construct($app);
    $this->dao = new DAO\UserDAO($this->logger);
    $perfilDAO = new DAO\ProfileDAO($this->logger);
    $this->clientDAO = new DAO\ClientDAO($this->logger);
    $this->listPerfis = $perfilDAO->retrieveAllProfiles();
    $this->listaUsers = $this->dao->retrieveAllUsersWithProfile();
  }

  public function inicio($request, $response, $args) {

      return $this->container->view->render($response, Globals::$USUARIO_TEMPLATE,
        [ Globals::$USUARIOS => $this->listaUsers,
          Globals::$PERFIS => $this->listPerfis
        ]);
  }
  public function inicioMinhaContaEdicao($request, $response, $args) {
      return $this->container->view->render($response, Globals::$MINHA_CONTA_EDICAO_TEMPLATE, [ ]);
  }

  public function inicioMinhaConta($request, $response, $args) {

      return $this->container->view->render($response, Globals::$MINHA_CONTA_TEMPLATE, [ ]);
  }

  public function cadastro($request, $response, $args) {
      unset($_SESSION[Globals::$USUARIO_CADASTRO]);
      return $this->container->view->render($response, Globals::$CADASTRO_TEMPLATE,
        [Globals::$PERFIS => $this->listPerfis]);
  }

  public function gravar($request, $response){
     $usuario = $request->getParsedBody();
     $usuario = json_decode(json_encode($usuario), FALSE);
     //var_dump($usuario);die;
     @$isEdicao = $usuario->id != "";

     $error = $this->validaForm($usuario);
     if(!$error) {
       $msg = $this->msgEmailJaExiste;
       $usuarioTemp = $this->dao->retrieveUserByEmail($usuario->email);
       if(!empty($usuarioTemp)) {
         if(!$isEdicao){//Novo
           $this->mensagemErroWeb($msg);
           $error = true;
         } else {//Edição
           $usuarioTemp = $usuarioTemp[0];
           if($usuarioTemp->idusuario != $usuario->idusuario){
             $this->mensagemErroWeb($msg);
             $error = true;
           }
         }
       }
    }

     //se passou toda validacao
     //verifica se for minha conta, se a senha esta correta
     //se nao for minha conta verifica se a senha vai ser modificada
     if(!$error && isset($usuario->editarMinhaConta)) {
       if(!$this->validaSenha($usuario)) {
         $this->mensagemErroWeb("Invalid password. It is not possible alter your account.");
         $error = true;
       } else {
         $usuario->idperfil = $_SESSION[Globals::$USUARIO_SESSAO]->idperfil;

       }
     }

     if(!$error) {
       //se passou toda validacao
       $usuarioTemp = $this->dao->retrieveUserByEmail($usuario->email);

       //faz cadastro novo usuário
       if(!$isEdicao){
         $usuario->password = Utils::encryptPassword($usuario->password);
         $usuario->date_recorded = date(Globals::$DATA_FORTMATO_DB_PADRAO);
         if(empty($usuarioTemp)) {
           $msgSucesso = "User successfully saved.";
           $novoId = $this->dao->insertUser($usuario);
           if($novoId != "") {
              $this->mensagemSucessoWeb($msgSucesso);
              unset($_SESSION[Globals::$USUARIO_CADASTRO]);
            } else {
              $this->mensagemErroWeb("There was an error on saving this user.");
              $error = true;
           }
         } else {
           // ja existe usuários
           $this->mensagemErroWeb($this->msgEmailJaExiste);
           $error = true;
         }
       } else {
          //edita usuário existente
          $usuarioTemp = $usuarioTemp[0];
           if(!empty($usuarioTemp) && $usuario->id != $usuarioTemp->id) {
             $this->mensagemErroWeb($this->msgEmailJaExiste);
             $error = true;
          } else {
            if($usuario->password != "") {
              $usuario->password = Utils::encryptPassword($usuario->password);
            } else {
              $usuario->password = $usuarioTemp->password;
            }
            $id = $this->dao->updateUser($usuario);
            if($id != ""){
              $this->mensagemSucessoWeb("User updated successfully.");
              unset($_SESSION[Globals::$USUARIO_CADASTRO]);
              if(isset($usuario->editarMinhaConta)) {
                $this->atualizaUserSessao($usuario->id);
              }
            } else {
              $this->mensagemErroWeb("There was an error on updating this user.");
              $error = true;
            }
          }
       }
     } else {
       $_SESSION[Globals::$USUARIO_CADASTRO] = $usuario;
     }

     $origem = $_SERVER['HTTP_REFERER'];
     if (strpos($origem, 'user') !== false) {
         $url = "/user";
     } else if(strpos($origem, 'minha-conta') !== false) {
        $url = "/my-account";
     } else {
         if($isEdicao && $error){
           $url = (Globals::$USUARIO_URL) . '/' . $usuario->id;
         } else {
           $url = Globals::$USUARIO_URL;
         }
     }
     return $response->withStatus(200)->withHeader(Globals::$LOCATION, $url);
   }

   private function montaApelido($string){
     $nome = explode(" ", $string);
     return substr($nome[0], 0, 15);
   }

   public function edita($request, $response, $args){
        @$usuario->id = $args['id'];
        $usuario = $this->dao->retrieveUserById($usuario->id);

       if(empty($usuario) || count($usuario) <= 0) {
         $this->mensagemErroWeb("User not found.");
       } else {
         $_SESSION[Globals::$USUARIO_CADASTRO]=$usuario[0];
       }
       return $this->container->view->render($response, Globals::$USUARIO_TEMPLATE,
         [ Globals::$USUARIOS => $this->listaUsers,
           Globals::$PERFIS => $this->listPerfis,
         ]);
   }

   public function apaga($request, $response, $args){
     if($this->dao->deleteUser($args['id'])){
         $this->mensagemSucessoWeb("User successfully deleted.");
         unset($_SESSION[Globals::$USUARIO_CADASTRO]);
       } else {
         $this->mensagemErroWeb("There was an error on deleting this user.");
       }
       return $response->withStatus(200)->withHeader(Globals::$LOCATION, Globals::$USUARIO_URL);
   }

   public function ativa($request, $response, $args) {
       if($this->dao->changeUserStatus($args['id'], 1)){
         $this->mensagemSucessoWeb("User successfully activated.");
       } else {
         $this->mensagemErroWeb("There was an error on activating this user.");
       }
       return $response->withStatus(200)->withHeader(Globals::$LOCATION, Globals::$USUARIO_URL);
   }

   public function inativa($request, $response, $args) {
     if($this->dao->changeUserStatus($args['id'], 0)){
       $this->mensagemSucessoWeb("User successfully .");
     } else {
       $this->mensagemErroWeb("There was an error on disactivating this user.");
     }
     return $response->withStatus(200)->withHeader(Globals::$LOCATION, Globals::$USUARIO_URL);
   }

   private function validaForm($usuario) {
     $error = false;
     if(!isset($usuario->username) || $usuario->username == "") {
       $this->mensagemErroWeb("Please, fill your name.");
       $error = true;
     }

     if(!isset($usuario->email) ||$usuario->email == "") {
        $this->mensagemErroWeb("Please, fill your email.");
        $error = true;
     } else {
       if(!Utils::validaEmail($usuario->email)){
         $this->mensagemErroWeb("E-mail's format is invalid.");
       }
     }

     if(!isset($usuario->id) ||$usuario->id == ""){
       if(!isset($usuario->password) || $usuario->password == "") {
          $this->mensagemErroWeb("Please, fill your password.");
          $error = true;
       } else if(strlen($usuario->password) < 6) {
          $this->mensagemErroWeb("Password cannot have less than 6 characters.");
          $error = true;
       } else if(!isset($usuario->repeatPassword) || $usuario->repeatPassword == "") {
          $this->mensagemErroWeb("Confirm your password.");
          $error = true;
       } else if($usuario->password != $usuario->repeatPassword) {
          $this->mensagemErroWeb("Passwords are different.");
          $error = true;
       }
     } else {
       if(isset($usuario->password) && !$usuario->password == "") {
         if(strlen($usuario->password) < 6) {
           $this->mensagemErroWeb("Password cannot have less than 6 characters.");
           $error = true;
         } else if(!isset($usuario->repeatPassword) || $usuario->repeatPassword == "") {
             $this->mensagemErroWeb("Confirm your password.");
             $error = true;
         } else if($usuario->password != $usuario->repeatPassword) {
             $this->mensagemErroWeb("Passwords are different.");
             $error = true;
         }
       }
     }

     if(!isset($usuario->id_profile) || $usuario->id_profile == "") {
         $this->mensagemErroWeb("Please, choose a profile.");
         $error = true;
     }
     return $error;
   }

   private function validaSenha($usuario) {
     return $this->dao->verifypassword($usuario);
   }

   private function atualizaUserSessao($usuarioid) {
     $usuarioAtualizado = $this->dao->retrieveUserAndProfileById($usuarioid);
     $usuarioAtualizado = $usuarioAtualizado[0];
     $permissoes = $_SESSION[Globals::$USUARIO_SESSAO]->urlsPermitidas;
     $usuarioAtualizado->senha = "";
     $usuarioAtualizado->urlsPermitidas = $permissoes;

     $_SESSION[Globals::$USUARIO_SESSAO] = $usuarioAtualizado;
   }
}
 ?>

<?php
namespace furnitureStore\Controller;
use furnitureStore\DAO;
use furnitureStore\Utils\Globals;
use furnitureStore\Utils\Utils;

  class LoginController extends BaseController {

    private $dao;
    private $daoFunc;

    public function __construct($app) {
      parent::__construct($app);
      $this->dao = new DAO\UserDAO($this->logger);
      $this->daoFunc = new DAO\FunctionalityDAO($this->logger);
    }

    public function inicio($request, $response, $args){
        if(isset($_SESSION[Globals::$USUARIO_SESSAO]) && !empty($_SESSION[Globals::$USUARIO_SESSAO])) {
            return $response->withStatus(200)->withHeader(Globals::$LOCATION, Globals::$ADMIN_INICIO_URL);
        }
        return $this->container->view->render($response, Globals::$LOGIN_TEMPLATE);
    }

    public function entrar($request, $response, $args){
        $user = $request->getParsedBody();
        $user = json_decode(json_encode($user), FALSE);

        $userEncontrado = $this->dao->recoverLogin($user);

        if(!$userEncontrado || empty($userEncontrado)) {
          $this->mensagemErroWeb("E-mail ou senha inválidos.");
          return $response->withStatus(200)->withHeader(Globals::$LOCATION, Globals::$LOGIN_URL);
        } else {
          if($userEncontrado->status == 0){
            $this->mensagemErroWeb("Sua conta não está ativa ainda. Aguarde um contato dos nossos consultores.");
            return $response->withStatus(200)->withHeader(Globals::$LOCATION, Globals::$LOGIN_URL);
          } else {
            //var_dump($userEncontrado);
            $lista = $this->daoFunc->retrievePermissionsByProfileId($userEncontrado->id_profile);
            //var_dump($lista); die;
            $userEncontrado->urlsPermitidas = $lista;
            $_SESSION[Globals::$USUARIO_SESSAO] = $userEncontrado;
            $this->mensagemSucessoWeb("Bem vindo de volta " . $userEncontrado->name);
            return $response->withStatus(200)->withHeader(Globals::$LOCATION, Globals::$ADMIN_INICIO_URL);
          }
        }
    }

    public function sair($request, $response, $args){
      unset($_SESSION[Globals::$USUARIO_SESSAO]);
      return $response->withStatus(200)->withHeader(Globals::$LOCATION, "/");
    }

    public function adminInicio($request, $response) {
      if(isset($_SESSION[Globals::$USUARIO_SESSAO]) && !empty($_SESSION[Globals::$USUARIO_SESSAO])) {
          return $this->container->view->render($response, Globals::$ADMIN_INICIO_TEMPLATE);
      }
      return $response->withStatus(200)->withHeader(Globals::$LOCATION, Globals::$LOGIN_URL);
    }

    public function inicioRecuperaSenha($request, $response) {
      return $this->container->view->render($response, Globals::$RECUPERA_SENHA_TEMPLATE);
    }

    public function recuperaSenha($request, $response) {
      $user = $request->getParsedBody();
      $user = json_decode(json_encode($user), FALSE);
      $userTemp = $this->dao->recuperaUserPorEmail($user->email);
      $url = Globals::$RECUPERA_SENHA_URL;
      if($userTemp != null && !empty($userTemp)){
        $userTemp = $userTemp[0];
        //gera nova senha e insere no banco
        $novasenha = Utils::generatePassword(10);
        $userTemp->senha = Utils::encryptPassword($novasenha);
        if($this->dao->mudaSenhaUser($userTemp)) {
          $mailDAO = new DAO\MailDAO($this->logger);
          $emailInfo = new \stdClass();
          $emailInfo->corpo = $this->container->view->fetch(Globals::$RECUPERA_SENHA_EMAIL_TEMPLATE,
                  ['novasenha' =>$novasenha, 'nome' => $userTemp->nome]);
          $emailInfo->destinatario = $userTemp;
          $emailInfo->titulo = "Recuperacao de senha website";
          if($mailDAO->enviaEmail($emailInfo)) {
            $this->mensagemSucessoWeb("Nova senha enviada com sucesso. Verifique seu e-mail cadastrado.");
            $url = Globals::$LOGIN_URL;
          }
        } else {
            $this->mensagemErroWeb("Houve um erro, por favor contate o administrador.");
            $this->logger->error("Erro ao atualizar senha do user: " . $userTemp->iduser);
        }
      } else {
        $this->mensagemErroWeb("Usuário não encontrado.");
      }
      return $response->withStatus(200)->withHeader(Globals::$LOCATION, $url);
    }
  }

 ?>

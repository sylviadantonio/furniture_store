<?php
namespace furnitureStore\Controller\Client;
use furnitureStore\Controller\BaseController;
use furnitureStore\DAO;
use furnitureStore\Utils\Globals;
use furnitureStore\Utils\Utils;

class ClientListController extends BaseController {

  private $dao;
  private $daoUser;
  private $daoAddress;

  public function __construct($app) {
    parent::__construct($app);
    $this->dao = new DAO\ClientDAO($this->logger);
    $this->daoUser = new DAO\UserDAO($this->logger);
    $this->daoAddress = new DAO\AddressDAO($this->logger);
  }

  public function start($request, $response, $args){
    $clientList = $this->dao->getAllClientsPartialInfo();
    return $this->container->view->render($response, Globals::$CLIENT_LIST_TEMPLATE, [Globals::$CLIENT_LIST => $clientList ]);
  }

  public function getUserInfo($request, $response, $args) {
    $id_client = $args['id'];
    $client = $this->dao->getCompleteClientById($id_client);
    $client[0]->password = "";
    return json_encode($client[0]);
  }

  public function delete( $request, $response, $args ) {
    $id_client = $args['id'];
    $client = $this -> dao -> getClient( $id_client );

    if( $this -> dao -> inactivateClientAndUser( $id_client, $client[0] -> id_user ) ) {
      $this -> mensagemSucessoWeb( "Client inactivated successfully." );
    } else {
      $this -> mensagemErroWeb( "Oops, there was an error on inactivating this client." );
    }
    return $response->withStatus(200)->withHeader(Globals::$LOCATION, Globals::$CLIENT_LIST_URL);
  }
}

<?php
namespace furnitureStore\Controller\Client;
use furnitureStore\Controller\BaseController;
use furnitureStore\DAO;
use furnitureStore\Utils\Globals;
use furnitureStore\Utils\Utils;

class ClientFormController extends BaseController {

  private $dao;
  private $daoUser;
  private $daoAddress;

  public function __construct($app) {
    parent::__construct($app);
    $this->dao = new DAO\ClientDAO($this->logger);
    $this->daoUser = new DAO\UserDAO($this->logger);
    $this->daoAddress = new DAO\AddressDAO($this->logger);
  }

  public function start($request, $response, $args){
    return $this->container->view->render($response, Globals::$CLIENT_FORM_TEMPLATE, [ ]);
  }

  public function insert($request, $response) {
    $client = $request->getParsedBody();
    $client = json_decode(json_encode($client), FALSE);
    $isEdicao = $client -> id_client != "";
    $url = Globals::$CLIENT_FORM_URL;

    $error = $this -> formValidation( $client );
    if( !$error ) {
      $client -> postal_code = strtoupper( $client -> postal_code );
      $client -> province = strtoupper( $client -> province );
      if( $isEdicao ) {
        if(!$this -> saveEdition( $client )) {            
          $url = Globals::$CLIENT_EDIT_URL ."". $client -> id_client;
        }
      } else {
        // insert USER
        $client -> password = Utils::encryptPassword( $client->password );
        $client -> date_recorded = date( Globals::$DATA_FORTMATO_DB_PADRAO );
        $userId = $this -> daoUser -> insertUser( $client );
        if( $userId != null && $userId != "" ) {
            // insert Address
            $addressId = $this -> daoAddress -> insertAddress( $client );
            if( $addressId != null && $addressId != "" ) {
              // insert client
              $client -> id_user = $userId;
              $client -> id_address = $addressId;
              $client -> status = 1;
              $clientId = $this -> dao -> insertClient($client);

              if( $clientId != null && $clientId != "") {
                $this -> mensagemSucessoWeb( "Client successfully inserted." );
              } else {
                $this -> mensagemErroWeb( "There was an error on inserting the client." );
                $error = true;
              }
            } else {
                $this -> mensagemErroWeb( "There was an error on inserting the address." );
                $error = true;
            }
        } else {
          $this -> mensagemErroWeb( "There was an error on inserting the user." );
          $error = true;
        }
      }
      if( $error ) {
        $_SESSION[Globals::$CLIENT] = $client;
      } else {
        unset($_SESSION[Globals::$CLIENT]);
      }
    }
    return $response->withStatus(200)->withHeader(Globals::$LOCATION, $url);
  }

  public function edit($request, $response, $args) {
    $id = $args['id'];
    $client = $this -> dao -> getCompleteClientById( $id );
    $_SESSION[Globals::$CLIENT]=$client[0];

    return $this->container->view->render($response, Globals::$CLIENT_FORM_TEMPLATE, [ ]);
  }

  public function formValidation( $client ) {
    $error = false;
    if( !isset( $client -> username ) || $client -> username == "" ) {
        $this -> mensagemErroWeb( "Username is mandatory." );
        $error = true;
    }
    if( !isset( $client -> email ) || $client -> email == "" ) {
      $this -> mensagemErroWeb( "E-mail is mandatory." );
      $error = true;
    } else {
      if( !Utils::validaEmail( $client -> email ) ) {
        $this -> mensagemErroWeb( "E-mail format is invalid." );
        $error = true;
      }
    }

    if( !isset( $client -> id_client ) || $client -> id_client == "" ){
      if( !isset( $client -> password ) || $client -> password == "" ) {
         $this -> mensagemErroWeb( "Password is mandatory." );
         $error = true;
      } else if( strlen( $client -> password ) < 6 ) {
         $this -> mensagemErroWeb( "Password cannot have less than 6 characters." );
         $error = true;
      } else if( !isset( $client -> repeatPassword ) || $client -> repeatPassword == "" ) {
         $this -> mensagemErroWeb( "Please, confirm your password." );
         $error = true;
      } else if( $client -> password != $client -> repeatPassword ) {
         $this -> mensagemErroWeb( "Passwords are different." );
         $error = true;
      }
    } else {
      if( isset( $client -> password ) && !$client -> password == "" ) {
        if( strlen( $client -> password ) < 6 ) {
          $this -> mensagemErroWeb( "Password cannot have less than 6 characters." );
          $error = true;
        } else if( !isset( $client -> repeatPassword ) || $client->repeatPassword == "" ) {
            $this -> mensagemErroWeb( "Confirm your password." );
            $error = true;
        } else if( $client -> password != $client -> repeatPassword ) {
            $this -> mensagemErroWeb( "Passwords are different." );
            $error = true;
        }
      }
    }

    if( !isset( $client -> name ) || $client -> name == "" ) {
        $this -> mensagemErroWeb( "Name is mandatory." );
        $error = true;
    }
    if( !isset( $client -> phone_1 ) || $client -> phone_1 == "" ) {
        $this -> mensagemErroWeb( "Telephone 1 is mandatory." );
        $error = true;
    }

    if( !isset( $client -> title ) || $client -> title == "" ) {
        $this -> mensagemErroWeb( "Address title is mandatory." );
        $error = true;
    }

    if( !isset( $client -> street ) || $client -> street == "" ) {
        $this -> mensagemErroWeb( "Street is mandatory." );
        $error = true;
    }

    if( !isset( $client -> number ) || $client -> number == "" ) {
        $this -> mensagemErroWeb( "Number is mandatory." );
        $error = true;
    }

    if( !isset( $client -> postal_code ) || $client -> postal_code == "" ) {
        $this -> mensagemErroWeb( "Postal Code is mandatory." );
        $error = true;
    }
    if( !isset( $client -> city ) || $client -> city == "" ) {
        $this -> mensagemErroWeb( "City is mandatory." );
        $error = true;
    }
    if( !isset( $client -> province ) || $client -> province == "" ) {
        $this -> mensagemErroWeb( "Province is mandatory." );
        $error = true;
    }
    return $error;
  }


  private function saveEdition ( $client ) {

    if( isset( $client -> password ) && $client -> password != "") {
      $client -> password = Utils::encryptPassword( $client -> password );
    } else {
      $clientTemp = $this -> dao -> getCompleteClientById($client -> id_client);
      $client -> password = $clientTemp[0] -> password;
    }
    $client -> status = 1;
    // update user
    if( $this -> daoUser -> updateUser( $client ) ) {
      if( $this -> daoAddress -> updateAddress( $client ) ) {
        if( $this -> dao -> updateClient( $client ) ) {
          $this -> mensagemSucessoWeb( "Client updated successfully." );
          return true;
        } else {
          $this -> mensagemErroWeb( "There was an error on updating client." );
        }
      } else {
        $this -> mensagemErroWeb( "There was an error on updating address." );
      }
    } else {
      $this -> mensagemErroWeb( "There was an error on updating user." );
    }
    return false;
  }
}

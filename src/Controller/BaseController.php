<?php
namespace furnitureStore\Controller;
use furnitureStore\DAO;

class BaseController {
    public $container;
    public $logger;

    public function __construct($app){
        $this->container = $app;
        $this->logger = $this->container->logger;
    }

    public function limpaString($string) {
  		$caracteres = array(".", "-", "/", "_", "(", ")", "+");
  		return str_replace($caracteres, "", $string);
  	}

    protected function getHeaderGet(){
        $header = array();
        $header[] = 'Content-type: application/json';
        $header[] = 'Accept: application/json';
        return $header;
    }

    protected function mensagemSucessoWeb($mensagem){
  		$this->container->flash->addMessage('sucess', $mensagem);
  	}

  	protected function mensagemErroWeb($mensagem){
  		$this->container->flash->addMessage('error', $mensagem);
  	}

    public function arrumaDataParaBanco($data){
        if($data != ""){
          $dataT = explode("/", $data);
          return $dataT[2]."-".$dataT[1]."-".$dataT[0];
        } else {
          return null;
        }
    }

    public function arrumaDataBanco($data){
        if($data != ""){
          $dataT = explode("-", $data);
          return $dataT[2]."/".$dataT[1]."/".$dataT[0];
        } else {
          return "";
        }
    }

    protected function mensagemErro($mensagem, $response, $httpCode = 400){
        return $response
            ->withStatus($httpCode)
            ->write(json_encode([
                "codigo" => $httpCode,
                "mensagem" => $mensagem
            ]));
    }
}

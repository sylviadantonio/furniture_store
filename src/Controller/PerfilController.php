<?php
namespace furnitureStore\Controller;
use furnitureStore\DAO;
use furnitureStore\Utils\Globals;

class PerfilController extends BaseController {
  private $dao;

  public function __construct($app) {
    parent::__construct($app);
    $this->dao = new DAO\ProfileDAO($this->logger);
  }

  public function inicio($request, $response, $args){
      $listaPerfis = $this->dao->retrieveAllProfiles();

      return $this->container->view->render($response, Globals::$PERFIL_TEMPLATE, [Globals::$PERFIS => $listaPerfis]);
  }

  public function edita($request, $response, $args){
      @$perfil->id = $args['id'];
      $perfil = $this->dao->retrieveProfilePorId($perfil->id);
      if($perfil == "" || count($perfil) <= 0){
        $this->mensagemErroWeb("Profile not found.");
      } else {
        $perfil = $perfil[0];
      }
      $listaPerfis = $this->dao->retrieveAllProfiles();
      return $this->container->view->render($response, Globals::$PERFIL_TEMPLATE, [Globals::$PERFIS => $listaPerfis, Globals::$PERFIL => $perfil]);
  }

  public function apaga($request, $response, $args){
      $id =$args['id'];

      if($this->dao->checkUserByProfile($id)) {
        $this->mensagemErroWeb("There are users associated to this profile. Is not possible to delete it.");

      } else {
        if($this->dao->deleteProfile($id)){
          $this->mensagemSucessoWeb("Profile successfully deleted.");
        } else {
          $this->mensagemErroWeb("Oops, there was an error deleting this profile.");
        }
      }
      return $response->withStatus(200)->withHeader(Globals::$LOCATION, Globals::$PERFIL_URL);
  }

  public function gravar($request, $response){
      $perfil = $request->getParsedBody();
		  $perfil = json_decode(json_encode($perfil), FALSE);
      if(!isset($perfil->name) || $perfil->name == ""){
        $msg = "Is necessary to fill the profile name.";
        $this->mensagemErroWeb($msg);
        return $response->withStatus(200)->withHeader(Globals::$LOCATION, Globals::$PERFIL_URL);
      }
      $perfilTemp = $this->dao->recoverProfileByName($perfil->name);
      if(!empty($perfilTemp)){
        $perfilTemp = $perfilTemp[0];
      } else {
        $perfilTemp = null;
      }
      if($perfil->id == ""){//Novo perfil
        if(!empty($perfilTemp)){
          $this->mensagemErroWeb("There is already a profile using this name.");
        } else {
          $id = $this->dao->insertProfile($perfil->name);
          if($id != ""){
            $this->mensagemSucessoWeb("Profile successfully inserted.");
          } else {
            $this->mensagemErroWeb("Oops, something happened when inserting the new profile.");
          }
        }
      } else {//Edição
        if(!empty($perfilTemp) && ($perfil->id != $perfilTemp->id)){
          $this->mensagemErroWeb("There is already a profile using this name.");
        } else {
          $id = $this->dao->updateProfile($perfil);
          if($id != ""){
            $this->mensagemSucessoWeb("Profile successfully updated.");
          } else {
            $this->mensagemErroWeb("Oops, something happened when updating this profile.");
          }
        }
      }
      return $response->withStatus(200)->withHeader(Globals::$LOCATION, Globals::$PERFIL_URL);
  }

}
?>

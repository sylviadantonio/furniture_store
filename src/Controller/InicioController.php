<?php
namespace furnitureStore\Controller;
use furnitureStore\DAO;

class InicioController extends BaseController {
  public function inicio($request, $response, $args){
      return $this->container->view->render($response, 'index.html', []);
  }
}
?>

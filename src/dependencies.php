<?php

use Slim\App;

return function (App $app) {
    $container = $app->getContainer();

    // view renderer
    $container['renderer'] = function ($c) {
        $settings = $c->get('settings')['renderer'];
        return new \Slim\Views\PhpRenderer($settings['template_path']);
    };

    $container['flash'] = function () {
        return new \Slim\Flash\Messages();
    };

    $container['view'] = function ($c) {
        $settings = $c->get('settings')['view'];
        $view = new \Slim\Views\Twig($settings['template_path'], [
            'cache' => false,
            'debug' => true // TODO: comentar em producao
        ]);

        if (isset($_SESSION['usuario'])) {
            $view->getEnvironment()->addGlobal("usuario", $_SESSION['usuario']);
        }
          if(isset($_SERVER["PRD"]) && $_SERVER["PRD"] == "true"){
            $view->getEnvironment()->addGlobal("server", "prd");
          }

        $view->addExtension(new Knlv\Slim\Views\TwigMessages(
            new Slim\Flash\Messages()
        ));

        $router = $c->get('router');
        $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
        $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));
        //TODO:comentar em producao
        $view->addExtension(new \Twig_Extension_Debug());
        $view->getEnvironment()->addGlobal("flash", $c['flash']->getMessages());
        $view->getEnvironment()->addGlobal("session", $_SESSION);

        return $view;
    };

    $container['logger'] = function ($c) {
        $settings = $c->get('settings')['logger'];
        $logger = new \Monolog\Logger($settings['name']);
        $logger->pushProcessor(new \Monolog\Processor\UidProcessor());
        $logger->pushHandler(new \Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
        return $logger;
    };
};

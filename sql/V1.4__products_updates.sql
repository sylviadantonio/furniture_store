INSERT INTO functionality (id_functionality, url, name, show_menu, id_menu_category) VALUES(12, '/client','Client', 0, 2);
INSERT INTO functionality (id_functionality, url, name, show_menu, id_menu_category) VALUES(13, '/tag','Tags', 1, 3);
INSERT INTO functionality (id_functionality, url, name, show_menu, id_menu_category) VALUES(14, '/category','Categories', 1, 3);

INSERT INTO access_permission (id_functionality, id_profile) VALUES(12, 1);
INSERT INTO access_permission (id_functionality, id_profile) VALUES(12, 2);

INSERT INTO access_permission (id_functionality, id_profile) VALUES(13, 1);
INSERT INTO access_permission (id_functionality, id_profile) VALUES(13, 2);

INSERT INTO access_permission (id_functionality, id_profile) VALUES(14, 1);
INSERT INTO access_permission (id_functionality, id_profile) VALUES(14, 2);

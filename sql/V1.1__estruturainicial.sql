-- -----------------------------------------------------
-- Table `profile`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `profile` (
  `id_profile` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_profile`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `user` (
  `id_user` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `password` VARCHAR(100) NOT NULL,
  `id_profile` INT NOT NULL,
  `status` INT DEFAULT 1 NOT NULL,
  `date_recorded` DATETIME NOT NULL,
  PRIMARY KEY (`id_user`),
  INDEX `fk_user_profile1_idx` (`id_profile` ASC),
  CONSTRAINT `fk_user_profile1`
    FOREIGN KEY (`id_profile`)
    REFERENCES `profile` (`id_profile`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `menu_category` (
  `id_menu_category` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `position` INT NOT NULL,
  PRIMARY KEY (`id_menu_category`))
ENGINE = InnoDB;

CREATE TABLE `functionality` (
  `id_functionality` int NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `show_menu` TINYINT NOT NULL DEFAULT '1',
  `id_menu_category` INT NULL DEFAULT NULL,
  PRIMARY KEY (`id_functionality`),
  INDEX `fk_funcionalidade_menu_category_idx` (`id_menu_category` ASC),
  CONSTRAINT `fk_rotina_menu_category`
    FOREIGN KEY (`id_menu_category`)
    REFERENCES `menu_category` (`id_menu_category`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION

) ENGINE=InnoDB;


CREATE TABLE `access_permission` (
  `id_access_permission` INT NOT NULL AUTO_INCREMENT,
  `id_functionality` INT NOT NULL,
  `id_profile` INT NOT NULL,
  PRIMARY KEY (`id_access_permission`),
  KEY `fk_access_permission_funcionalidade1_idx` (`id_functionality`),
  KEY `fk_access_permission_profile1_idx` (`id_profile`),
  CONSTRAINT `fk_access_permission_funcionalidade1` FOREIGN KEY (`id_functionality`) REFERENCES `functionality` (`id_functionality`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_access_permission_profile1` FOREIGN KEY (`id_profile`) REFERENCES `profile` (`id_profile`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;

CREATE TABLE `logerror` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `sqlerror` text COLLATE utf8_unicode_ci,
  `datetime` datetime DEFAULT NULL,
  `action` text COLLATE utf8_unicode_ci,
  `message` text COLLATE utf8_unicode_ci,
  `datelastchange` datetime DEFAULT NULL,
  `resolved` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

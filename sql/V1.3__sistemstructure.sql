-- -----------------------------------------------------
-- Table `address`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `address` ;

CREATE TABLE IF NOT EXISTS `address` (
  `id_address` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(100) NOT NULL,
  `street` VARCHAR(255) NOT NULL,
  `number` VARCHAR(5) NOT NULL,
  `complement` VARCHAR(45) NULL,
  `postal_code` VARCHAR(7) NOT NULL,
  `city` VARCHAR(255) NOT NULL,
  `province` VARCHAR(2) NOT NULL,
  PRIMARY KEY (`id_address`))
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `client`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `client` ;

CREATE TABLE IF NOT EXISTS `client` (
  `id_client` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `phone_1` VARCHAR(25) NOT NULL,
  `phone_2` VARCHAR(25) NULL,
  `ext` VARCHAR(15) NULL,
  `id_address` INT NULL,
  `status` TINYINT NOT NULL DEFAULT 1,
  `id_user` INT NULL,
  PRIMARY KEY (`id_client`),
  INDEX `fk_address_1_idx` (`id_address` ASC) VISIBLE,
  INDEX `fk_user_idx` (`id_user` ASC) VISIBLE,
  CONSTRAINT `fk_address_1`
    FOREIGN KEY (`id_address`)
    REFERENCES `address` (`id_address`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user`
    FOREIGN KEY (`id_user`)
    REFERENCES `user` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `product`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `product` ;

CREATE TABLE IF NOT EXISTS `product` (
  `id_product` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `description` VARCHAR(500) NOT NULL,
  `friendly_url` VARCHAR(120) NOT NULL,
  `rental_qtt` INT NOT NULL,
  `sell_qtt` INT NOT NULL,
  `total_qtt` INT NOT NULL,
  `width` VARCHAR(45) NULL,
  `depth` VARCHAR(45) NULL,
  `height` VARCHAR(45) NULL,
  `original_price` FLOAT NOT NULL,
  `rent_code` VARCHAR(45) NULL,
  `rent_qr_code_img` VARCHAR(150) NULL,
  `inventory_n` VARCHAR(45) NULL,
  `inv_qr_code_img` VARCHAR(150) NULL,
  `status` VARCHAR(45) NULL,
  PRIMARY KEY (`id_product`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `product_price`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `product_price` ;

CREATE TABLE IF NOT EXISTS `product_price` (
  `id_product_price` INT NOT NULL AUTO_INCREMENT,
  `id_product` INT NOT NULL,
  `initial_sell_date` DATE NOT NULL,
  `sell_new_price` FLOAT NOT NULL,
  `sell_used_price` FLOAT NOT NULL,
  `initial_rent_date` DATE NOT NULL,
  `rent_weekly_price` FLOAT NOT NULL,
  PRIMARY KEY (`id_product_price`),
  INDEX `fk_id_product_idx` (`id_product` ASC) VISIBLE,
  CONSTRAINT `fk_id_product`
    FOREIGN KEY (`id_product`)
    REFERENCES `product` (`id_product`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `product_picture`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `product_picture` ;

CREATE TABLE IF NOT EXISTS `product_picture` (
  `id_product_picture` INT NOT NULL AUTO_INCREMENT,
  `id_product` INT NOT NULL,
  `img` VARCHAR(255) NOT NULL,
  `folder` VARCHAR(255) NOT NULL,
  `cover` TINYINT NOT NULL DEFAULT '1',
  `title` VARCHAR(155) NOT NULL,
  PRIMARY KEY (`id_product_picture`),
  INDEX `fk_product_idx` (`id_product` ASC) VISIBLE,
  CONSTRAINT `fk_product`
    FOREIGN KEY (`id_product`)
    REFERENCES `product` (`id_product`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `order_rent`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `order_rent` ;

CREATE TABLE IF NOT EXISTS `order_rent` (
  `id_order_rent` INT NOT NULL,
  `id_client` INT NOT NULL,
  `delivery` TINYINT NOT NULL DEFAULT 0,
  `id_deliver_address` INT NULL,
  `delivery_price` FLOAT NULL,
  `total_products` FLOAT NULL,
  `total_taxes` FLOAT NULL,
  `total_order` VARCHAR(45) NULL,
  `pickup` TINYINT NOT NULL DEFAULT 0,
  `id_pickup_address` INT NULL,
  `obs` VARCHAR(500) NULL,
  `order_closed` TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_order_rent`),
  INDEX `fk_client_2_idx` (`id_client` ASC) VISIBLE,
  INDEX `fk_address_2_idx` (`id_deliver_address` ASC) VISIBLE,
  INDEX `fk_pickup_address_idx` (`id_pickup_address` ASC) VISIBLE,
  CONSTRAINT `fk_client_2`
    FOREIGN KEY (`id_client`)
    REFERENCES `client` (`id_client`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_deliver_address`
    FOREIGN KEY (`id_deliver_address`)
    REFERENCES `address` (`id_address`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pickup_address`
    FOREIGN KEY (`id_pickup_address`)
    REFERENCES `address` (`id_address`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `order_sell`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `order_sell` ;

CREATE TABLE IF NOT EXISTS `order_sell` (
  `id_order_sell` INT NOT NULL AUTO_INCREMENT,
  `id_client` INT NULL,
  `delivery` TINYINT NOT NULL DEFAULT 0,
  `id_deliver_address` INT NULL,
  `delivery_price` FLOAT NULL,
  `total_products` FLOAT NOT NULL,
  `total_taxes` FLOAT NOT NULL,
  `total_order` FLOAT NOT NULL,
  PRIMARY KEY (`id_order_sell`),
  INDEX `fk_client_1_idx` (`id_client` ASC) VISIBLE,
  INDEX `fk_delivery_address_idx` (`id_deliver_address` ASC) VISIBLE,
  CONSTRAINT `fk_client_1`
    FOREIGN KEY (`id_client`)
    REFERENCES `client` (`id_client`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_delivery_address2`
    FOREIGN KEY (`id_deliver_address`)
    REFERENCES `address` (`id_address`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `order_rent_product`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `order_rent_product` ;

CREATE TABLE IF NOT EXISTS `order_rent_product` (
  `id_order_rent_product` INT NOT NULL AUTO_INCREMENT,
  `id_order` INT NOT NULL,
  `id_product` INT NOT NULL,
  `discount_percentage` FLOAT NULL DEFAULT 0.0,
  `price_single` FLOAT NOT NULL,
  `qtt` TINYINT NOT NULL,
  `total_price` FLOAT NOT NULL,
  `date_start` DATE NOT NULL,
  `date_end` DATE NOT NULL,
  `checked_out` TINYINT NOT NULL DEFAULT 0,
  `returned` TINYINT NOT NULL DEFAULT 0,
  `date_returned` DATE NULL,
  `returned_conditions` TEXT NULL,
  PRIMARY KEY (`id_order_rent_product`),
  INDEX `fk_order_idx` (`id_order` ASC) VISIBLE,
  INDEX `fk_product_1_idx` (`id_product` ASC) VISIBLE,
  CONSTRAINT `fk_order`
    FOREIGN KEY (`id_order`)
    REFERENCES `order_rent` (`id_order_rent`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_1`
    FOREIGN KEY (`id_product`)
    REFERENCES `product` (`id_product`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `order_sell_product`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `order_sell_product` ;

CREATE TABLE IF NOT EXISTS `order_sell_product` (
  `id_order_sell_product` INT NOT NULL AUTO_INCREMENT,
  `id_order` INT NOT NULL,
  `id_product` INT NOT NULL,
  `price_single` FLOAT NOT NULL,
  `discount_percentage` FLOAT NULL DEFAULT 0.0,
  `qtt` TINYINT NOT NULL,
  `total_price` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_order_sell_product`),
  INDEX `fk_order_id_idx` (`id_order` ASC) VISIBLE,
  INDEX `fk_product_2_idx` (`id_product` ASC) VISIBLE,
  CONSTRAINT `fk_order_id`
    FOREIGN KEY (`id_order`)
    REFERENCES `order_sell` (`id_order_sell`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_2`
    FOREIGN KEY (`id_product`)
    REFERENCES `product` (`id_product`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `category` ;

CREATE TABLE IF NOT EXISTS `category` (
  `id_category` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `id_cat_parent` INT NULL,
  PRIMARY KEY (`id_category`),
  INDEX `fk_parent_idx` (`id_cat_parent` ASC) VISIBLE,
  CONSTRAINT `fk_parent`
    FOREIGN KEY (`id_cat_parent`)
    REFERENCES `category` (`id_category`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tag`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tag` ;

CREATE TABLE IF NOT EXISTS `tag` (
  `id_tag` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_tag`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `category_product`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `category_product` ;

CREATE TABLE IF NOT EXISTS `category_product` (
  `id_category_product` INT NOT NULL AUTO_INCREMENT,
  `id_product` INT NOT NULL,
  `id_category` INT NOT NULL,
  PRIMARY KEY (`id_category_product`),
  INDEX `fk_prod_idx` (`id_product` ASC) VISIBLE,
  INDEX `fk_cat_idx` (`id_category` ASC) VISIBLE,
  CONSTRAINT `fk_prod`
    FOREIGN KEY (`id_product`)
    REFERENCES `product` (`id_product`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_cat`
    FOREIGN KEY (`id_category`)
    REFERENCES `category` (`id_category`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tag_product`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tag_product` ;

CREATE TABLE IF NOT EXISTS `tag_product` (
  `id_tag_product` INT NOT NULL AUTO_INCREMENT,
  `id_tag` INT NOT NULL,
  `id_product` INT NOT NULL,
  PRIMARY KEY (`id_tag_product`),
  INDEX `fk_tag_idx` (`id_tag` ASC) VISIBLE,
  INDEX `fk_prod_tag_idx` (`id_product` ASC) VISIBLE,
  CONSTRAINT `fk_tag`
    FOREIGN KEY (`id_tag`)
    REFERENCES `tag` (`id_tag`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_prod_tag`
    FOREIGN KEY (`id_product`)
    REFERENCES `product` (`id_product`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

INSERT INTO menu_category (id_menu_category, name, position) VALUES
(1, 'Admin', 1),
(2, 'Client Management', 2),
(3, 'Product Management', 3),
(4, 'Rental Management', 4),
(5, 'Selling Management', 5);


INSERT INTO functionality (id_functionality, url, name, show_menu) VALUES(1, '/admin','Home', 0);
INSERT INTO functionality (id_functionality, url, name, show_menu) VALUES(2, '/logout', 'Logout', 0);
INSERT INTO functionality (id_functionality, url, name, show_menu, id_menu_category) VALUES(3, '/user','User', 1, 1);
INSERT INTO functionality (id_functionality, url, name, show_menu, id_menu_category) VALUES(4, '/profile','Profile', 1, 1);
INSERT INTO functionality (id_functionality, url, name, show_menu, id_menu_category) VALUES(5,'/functionality', 'Functionality', 1, 1);

INSERT INTO functionality (id_functionality, url, name, show_menu, id_menu_category) VALUES(6, '/client','Add Client', 0, 2);
INSERT INTO functionality (id_functionality, url, name, show_menu, id_menu_category) VALUES(7, '/client/add','Add Client', 1, 2);
INSERT INTO functionality (id_functionality, url, name, show_menu, id_menu_category) VALUES(8 , '/client/list','List Client', 1, 2);

INSERT INTO functionality (id_functionality, url, name, show_menu, id_menu_category) VALUES(9, '/product','Product', 0, 3);
INSERT INTO functionality (id_functionality, url, name, show_menu, id_menu_category) VALUES(10, '/product/add','Add Product', 1, 3);
INSERT INTO functionality (id_functionality, url, name, show_menu, id_menu_category) VALUES(11 , '/product/list','List Products', 1, 3);


INSERT INTO profile (id_profile, name) VALUES(1, 'Admin');
INSERT INTO profile (id_profile, name) VALUES(2, 'Colaborator');
INSERT INTO profile (id_profile, name) VALUES(3, 'Customer');

--- admin page --
INSERT INTO access_permission (id_access_permission, id_functionality, id_profile) VALUES(1, 1, 1);
INSERT INTO access_permission (id_access_permission, id_functionality, id_profile) VALUES(2, 1, 2);

-- logout permission --
INSERT INTO access_permission (id_access_permission, id_functionality, id_profile) VALUES(3, 2, 1);
INSERT INTO access_permission (id_access_permission, id_functionality, id_profile) VALUES(4, 2, 2);
INSERT INTO access_permission (id_access_permission, id_functionality, id_profile) VALUES(5, 2, 3);

-- user page -- only admin
INSERT INTO access_permission (id_access_permission, id_functionality, id_profile) VALUES(6, 3, 1);

-- profile page -- only admin
INSERT INTO access_permission (id_access_permission, id_functionality, id_profile) VALUES(7, 4, 1);

-- functionality  page -- only admin
INSERT INTO access_permission (id_access_permission, id_functionality, id_profile) VALUES(8, 5, 1);

-- client page -- admin and colaborator
INSERT INTO access_permission (id_access_permission, id_functionality, id_profile) VALUES(9, 6, 1);
INSERT INTO access_permission (id_access_permission, id_functionality, id_profile) VALUES(10, 6, 2);

-- add client page -- admin and colaborator
INSERT INTO access_permission (id_access_permission, id_functionality, id_profile) VALUES(11, 7, 1);
INSERT INTO access_permission (id_access_permission, id_functionality, id_profile) VALUES(12, 7, 2);

-- client list permission --admin and colaborator
INSERT INTO access_permission (id_access_permission, id_functionality, id_profile) VALUES(13, 8, 1);
INSERT INTO access_permission (id_access_permission, id_functionality, id_profile) VALUES(14, 8, 2);

-- product permission -- admin and colaborator
INSERT INTO access_permission (id_access_permission, id_functionality, id_profile) VALUES(15, 9, 1);
INSERT INTO access_permission (id_access_permission, id_functionality, id_profile) VALUES(16, 9, 2);

-- product add permission -- admin and colaborator
INSERT INTO access_permission (id_access_permission, id_functionality, id_profile) VALUES(17, 10, 1);
INSERT INTO access_permission (id_access_permission, id_functionality, id_profile) VALUES(18, 10, 2);

-- product list permission -- admin and colaborator
INSERT INTO access_permission (id_access_permission, id_functionality, id_profile) VALUES(19, 11, 1);
INSERT INTO access_permission (id_access_permission, id_functionality, id_profile) VALUES(20, 11, 2);



INSERT INTO user (id_user, username, email, password, id_profile, date_recorded, status)
VALUES(1, 'Admin', 'admin@admin.com', '$2y$10$zmaaGil/CHMUIwKeqt1nG.yI/CPUjSj3W25ILSrOgoiqmsrpPjmu.', 1, '2020-08-21 00:00:00.000', 1);

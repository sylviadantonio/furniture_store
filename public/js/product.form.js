$(document).ready(function(){
  $.validity.setup({ outputMode:"summary" });
  $('#productForm').validity(validationProductForm);
  console.log('got here')
  $('#descricao').summernote();  
});
$("body").on("click", ".btexcluir", function(){
     var id = $(this).attr('id');
     var url = "/product/" + id;
     $("#excluiForm").attr('action', url).submit();
});

$("body").on("click", ".btn-rent-qr", function(){
  let code = $('#rent_code').val()
  let img = addQR(code, 'rent_code')
});

$("body").on("click", ".btn-inv-qr", function(){
  let code = $('#inventory_n').val()
  let img = addQR(code, 'inventory_n')
});

$("body").on("focusout", "#title", function(){
  if($('#friendly_url').val() =="") {
    generateFriendlyURL($(this).val())
  }
});
$("body").on("click", ".btn-edit-url", function(){
  $(this).text('Save')
  $('#friendly_url').attr('readonly', false)
  $(this).removeClass('btn-edit-url').addClass('btn-save-url')
});

$("body").on("click", ".btn-save-url", function(){
  generateFriendlyURL($('#friendly_url').val())
});


$("body").on("click", "#add-tag", function(){
  if( $('#search-tag').val() == ""  || $('#search-tag').val().length < 3) {
    new PNotify({
        title: 'Oops',
        text: 'You must fill the field tag and must be more then 3 characters.',
        hide: false,
        styling: 'bootstrap3',
        type: 'error'
    });
  } else {
    $.ajax({
      type: "POST",
      url: '/tag-async',
      dataType: 'json',
      data: {'name' : $('#search-tag').val()},
      success: function (result){
        if(result.success) {
          generateTagItem(result.success.id, result.success.name)
        } else {
          new PNotify({
              title: 'Oops',
              text: 'There was an error adding this new tag.',
              hide: false,
              styling: 'bootstrap3',
              type: 'error'
          });
        }
      },
      error: "alert(Oops, there was an error on processing your request.)"
    });
  }
})

const generateTagItem = function( id, name ) {
  let html = `
  <div class="tag-item">
    <span class="tag"><span>${name}&nbsp;&nbsp;</span><a href="#" title="Removing tag" class="remove-tag">x</a></span>
    <input type="hidden" name="tags[]" class="tags-input" value="${id}">
  </div>
  `
  $('#search-tag').val("")
  $('.tags-chosen').append(html)
}



  var tags =[];
  $('#search-tag').autocomplete({
    minChars: 1,
    triggerSelectOnValidInput: false,
    lookup: function (query, done) {
      var url = "/tag/search/" + query;
      $.get(url, function( data ) {
          tags = data
      });
        var result = {
            suggestions: tags
      };
      done( result );
    },
    onSelect: function (item) {
      generateTagItem(item.data, item.value)
    }
  });

  $("body").on("click", '.remove-tag', function(){
      $(this).parent().parent('.tag-item').remove()
  });

$("body").on("change", 'input[type="file"].gallery-pictures', function(e){
  $('.img-titles').html("")
  let list = e.target.files
  let html =''
  for(let x = 0; x < list.length ; x++) {
    html+=`
    <div class="form-group col-md-4 col-lg-4 col-sm-12 col-xs-12">
      <label class="form-label" for="depth">Title for image ${x+1}:</label>
      <input type="text" class="form-control" name="img_titles[]" value="${list[x].name}">
    </div>
    `
  }
  $('.img-titles').append(html);
});

$("body").on("change", 'input[type="file"].main-pic', function(e){
  $('.main-img-title').html("")

  let list = e.target.files
  let html =`
    <div class="form-group col-md-4 col-lg-4 col-sm-12 col-xs-12">
      <label class="form-label" for="depth">Title for main image:</label>
      <input type="text" class="form-control" name="main_img_title" value="${list[0].name}">
    </div>
  `
  $('.main-img-title').append(html);
});

const validationProductForm = function() {
  $('#title').require()
  $('#friendly_url').require()
  $('#total_qtt').require("Total Quantity is mandatory.")
  $('#rent_code').require("QR Code is Mandatory.")
}

const addQR = function ( code, inputName ) {
    $.ajax({
      type: "GET",
      url: '/product/qr/'+ code,
      dataType: 'json',
      success: function (address){
        $('.' + inputName ).val(code)
        $('#' + inputName).siblings('.address-img').val(address.url)
        $('#' + inputName).parent().siblings('.qr-code-img').append(`
          <img src="${address.url}" class="qr-img">
          `)
      },
      error: "alert(Oops, there was an error on processing your request.)"
    });
    $('#fulldetailsmodal').modal('show');

}

const generateFriendlyURL = function (title) {
  $.ajax({
    type: "GET",
    url: '/product/check-friedly-url/'+ title,
    success: function(result) {
      result = JSON.parse(result);
      $('#friendly_url').val(result.url)
      $('#friendly_url').attr('readonly', 'readonly')
      $('.btn-save-url').text('Edit')
      $('.btn-save-url').removeClass('btn-save-url').addClass('btn-edit-url')
    },
    error: "alert(Oops, there was an error on processing your request.)"
  })


}

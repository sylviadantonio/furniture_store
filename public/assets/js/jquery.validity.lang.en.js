$.extend($.validity.messages, {
    require:"#{field} is mandatory.",
    // Format validators:
	match:"#{field} has invalid format.",
	integer:"#{field} must be an integer.",
	date:"#{field} must be in the format: (dd/mm/yyyy)",
	email:"#{field} has an invalid format.",
	usd:"#{field} não está no formato monetário correto.",
	url:"#{field} não é uma URL válida.",
	number:"#{field} must contain just numbers.",
	zip:"#{field} não está no formato correto #####-###.",
	phone:"#{field} não está no formato correto ##-####-####.",
	guid:"#{field} must be formatted as a guid like {3F2504E0-4F89-11D3-9A0C-0305E82C3301}.",
	time24:"#{field} tem de estar no formato 24 horas por exemplo: 23:00.",
	time12:"#{field} must be formatted as a 12 hour time: 12:00 AM/PM",

   // Value range messages:
	lessThan:"#{field} deve ser menor do que #{max}.",
	lessThanOrEqualTo:"#{field} deve ser menor ou igual à #{max}.",
	greaterThan:"#{field} deve ser maior do que #{min}.",
	greaterThanOrEqualTo:"#{field} deve ser maior ou igual à #{min}.",
	range:"#{field} deve estar entre #{min} e #{max}.",

    // Value length messages:
	tooLong:"#{field} não pode ser maior do que #{max} caracteres.",
	tooShort:"#{field} não pode ser menor do que #{min} caracteres.",

	// Composition validators:
	nonHtml:"#{field} cannot contain HTML characters.",
	alphabet:"#{field} contains disallowed characters.",

	minCharClass:"#{field} não pode ter mais do que #{min} #{charClass} caracteres.",
	maxCharClass:"#{field} não pode ter menos do que #{min} #{charClass} caracteres.",

	// Aggregate validator messages:
	equal:"Valores devem ser iguais.",
	distinct:"Valores não podem ser repetidos.",
	sum:"Values don't add to #{sum}.",
	sumMax:"The sum of the values must be less than #{max}.",
	sumMin:"The sum of the values must be greater than #{min}.",

	// Radio validator messages:
	radioChecked:"O valor selecionado não é válido.",

	generic:"Inválido."
});

$.validity.setup({ defaultFieldName:"O campo", });

$.extend($.validity.patterns, {
	date:/^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/,

    zip: /^\d{3}$/,

    phone: /((8|\+7)-?)?\(?\d{3,5}\)?-?\d{1}-?\d{1}-?\d{1}-?\d{1}-?\d{1}((-?\d{1})?-?\d{1})?/
});

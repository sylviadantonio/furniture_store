$(document).ready(function(){
  $('.postal_code').mask('S0S 0S0');
  $('.phone').mask('(000) 000-0000');
  $('.ano').mask('0000');
  $('.data').mask('00/00/0000');
  $('.currency').mask("#.###.##0,00", {reverse: true});
});

const validationUser = function() {
  $("#username").require();
  $("#email").require().match('email');
  if($("#id").val() == "" || $("#password").val() != ""){
    $("#password").require().minLength(6);
    $("#repeatPassword").require("Please, repeat the password.").minLength(6);
    $("#password").assert($("#password").val() == $("#repeatPassword").val(), "Password and confirmation must matching.");
  }
  $("#profile").require()
}

const validationClient = function(){
  $("#username").require();
  $("#email").require().match('email');
  if($("#id").val() == "" || $("#password").val() != ""){
    $("#password").require().minLength(6);
    $("#repeatPassword").require("Please, repeat the password.").minLength(6);
    $("#password").assert($("#password").val() == $("#repeatPassword").val(), "Password and confirmation must matching.");
  }
  $("#name").require();
  $("#phone_1").require();
  $("#street").require();
  $("#number").require();
  $("#city").require();
  $("#postal_code").require();
  $("#province").require().minLength(2).maxLength(2);


}

const limpaFormularioUsuario = function(){
  $("#endereco").val("");
  $("#bairro").val("");
  $("#cidade").val("");
  $("#uf").val("");
  $("#numero").val("");
  $("#complemento").val("");
}


const setDeleteModal = function (id, msg) {
  $('#excluiMmodal .modal-footer .btexcluir').attr('id', id);
  $('#excluiMmodal .modal-body .texto-exclusao').text(msg);
  $('#excluiMmodal').modal('show');
}
